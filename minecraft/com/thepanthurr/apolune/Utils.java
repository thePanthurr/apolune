package com.thepanthurr.apolune;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.util.regex.Pattern;

import net.minecraft.src.AxisAlignedBB;
import net.minecraft.src.DynamicTexture;
import net.minecraft.src.Gui;
import net.minecraft.src.Minecraft;
import net.minecraft.src.ResourceLocation;
import net.minecraft.src.Tessellator;
import net.minecraft.src.TextureManager;

import org.lwjgl.opengl.GL11;

import com.thepanthurr.apolune.module.Module;

/**
 * Utils is a class for extraneous methods that
 * need to be referenced in multiple classes..
 *
 * @author thePanthurr/ThatNahr
 * @version 1.0.6
 * @since 4/15/13
 */
public class Utils {

    private static volatile Utils newInstance;                                      //A variable for holding the single instance of Utils.
    private File apoluneDir = new File(Minecraft.getMinecraft().mcDataDir + "/apolune");   //A variable for holding the directory of Apolune related files.
    private Gui gui = new Gui();                                                    //A variable for holding an instance of Gui for TypeFont to use.
    private int color = 0xff5500AA;                                                 //A variable for holding the color of the Client.
    private TypeFont defaultFont;                                                   //A variable to hold the font for most of the Client.
    private TypeFont headerFont;                                                    //A variable to hold the font for the headings of the Client.
    private TypeFont smallFont;                                                     //A variable to hold the font for the small parts of the Client.
    private Pattern colorCodes = Pattern.compile("(?i)\\u00A7[0-9A-FK-ORQ]");       //A variable to hold the color codes for chat and other things.

    /**
     * A Singleton method used to access the single instance of
     * Utils, it is double-locked and thread-safe.
     *
     * @return newInstance
     *         A variable of type Utils, only set to be other
     *         than null when this method is first called.
     */
    public static Utils getInstance() {
        if (newInstance == null) {
            synchronized (Utils.class) {
                if (newInstance == null) {
                    newInstance = new Utils();
                }
            }
        }
        return newInstance;
    }

    /**
     * A method used to get the directory of all Apolune
     * related files.
     *
     * @return apoluneDir
     *         The directory path in which to save all files relating to the mod.
     */
    public File getApoluneDir() {
        if (!apoluneDir.exists()) {
            apoluneDir.mkdir();
        }
        return apoluneDir;
    }
    
    /**
     * Method used to "fade" a value depending on a boolean.  This can be used for
     * stuff like animations.  It can also be used in the sense of Minecraft when making
     * a "fading" fullbright.
     * 
     * @author Knar.
     * @param value Value in which we add & subtract to.
     * @param inc Increment in which we add & subtract to & from value.
     * @param min Minimum value.
     * @param max Maximum value.
     * @param b Determines whether to add or subtract.
     */
    public void fadeValue(int value, int inc, int min, int max, boolean b) {
    	long lastFade = 0;
    	if(System.nanoTime() - lastFade >= 10) {
    		if(b) {
    			value += inc;
    		}else{
    			value -=inc;
    		}
    		lastFade = System.nanoTime();
    	}
    	if(value < min && !b) {
    		value = min;
    	}else if(value > max && b) {
    		value = max;
    	}    	
    }

    /**
     * This method returns a color code for Minecraft
     * to define how the clients main color should look.
     *
     * @return \247q
     *         The prefix defined in TypeFont.java that
     *         decides the color of the text inside it.
     */
    public String getPrefix() {
        return "\247q";
    }

    /**
     * This method returns the color code for Minecraft
     * to define how the clients main color Strings should
     * look.
     *
     * @return color
     *         The hexadecimal color code variable held in this class.
     */
    public int getColor() {
        return color;
    }

    /**
     * This method sets the hexadecimal color code variable
     * in this class to the specified integer, this is later
     * converted to RGB values, so that it may be utilized in
     * creating a custom font color, as well as being used to
     * color other Gui elements.
     *
     * @param color The integer that the color variable is going to be set to.
     */
    public void setColor(int color) {
        this.color = color;
    }

    /**
     * This method turns a hexadecimal
     * value into an rgba value.
     *
     * @param hex The hexadecimal color code that is to be converted.
     * @return new float[]{red, green, blue, alpha}
     *         A new float array containing 4 values for use with GL11.
     */
    public float[] getRGBFromHex(int hex) {
        float temp[] = new float[4];
        temp[0] = (hex >> 16 & 0xFF) / 255F;
        temp[1] = (hex >> 8 & 0xFF) / 255F;
        temp[2] = (hex & 0xFF) / 255F;
        temp[3] = (hex >> 24 & 0xFF) / 255F;
        for (int i = 0; i < temp.length; i++) {
            if (temp[i] == 0) {
                temp[i] = 0.01F;
            }
        }
        return temp;
    }

    /**
     * This method turns an rgba array of floats
     * to a hexadecimal value.
     *
     * @param rgba The float array of RGBA values that is going to be converted.
     * @return color.getAlpha() + color.getRGB()
     *         The integer value returned when adding the Alpha and RGB of the hexadecimal.
     */
    public int getHexFromRGB(float rgba[]) {
        Color color = new Color(rgba[0], rgba[1], rgba[2], rgba[3]);
        return color.getAlpha() + color.getRGB();
    }

    /**
     * This method gets a lighter or darker color
     * of the specified color, for use in gradients.
     *
     * @param color   The hexadecimal color code that is to be converted.
     * @param percent The percent of white added to the color, i.e lower values
     *                mean the color is darker, where as higher values make the
     *                color brighter
     * @return getHexFromRGB(new float[]{red, green, blue, alpha})
     *         Returns a float array containing red, green, blue, and alpha values
     *         for the new tinted color.
     * @see #getHexFromRGB(float[])
     */
    public int getTintedColor(int color, int percent) {
        float value = (float) percent / 100;
        float red = getRGBFromHex(color)[0] * value;
        float green = getRGBFromHex(color)[1] * value;
        float blue = getRGBFromHex(color)[2] * value;
        float alpha = getRGBFromHex(color)[3];

        return getHexFromRGB(new float[]{red, green, blue, alpha});
    }

    /**
     * This method capitalizes a string.
     *
     * @param string The string that is to be capitalized.
     * @return string.substring(0, 1).toUpperCase() + string.substring(1, string.length())
     *         The string is split into its first letter and the rest of its
     *         letters, the first letter is capitalized, and then added to the
     *         rest of the letters in the string.
     */
    public String capitalize(String string) {
        return string.substring(0, 1).toUpperCase() + string.substring(1, string.length());
    }

    /**
     * A method to get a generic Gui instance for font
     * rendering.
     *
     * @return gui
     *         A new gui instance defined at the top of the class.
     */
    public Gui getGui() {
        return gui;
    }

    /**
     * A method to prevent the creation of too many TTF fonts causing
     * a memory leak. Returns the default font for the Client.
     *
     * @return defaultFont
     *         The TTF for default fonts defined in a variable.
     */
    public TypeFont getDefaultFont() {
        if (defaultFont == null) {
            defaultFont = new TypeFont(new Font("Verdana", 8, Font.BOLD), 8);
        }

        return defaultFont;
    }

    /**
     * A method to prevent the creation of too many TTF fonts causing
     * a memory leak. Returns the header font for the Client.
     *
     * @return headerFont
     *         The TTF for header fonts defined in a variable.
     */
    public TypeFont getHeaderFont() {
        if (headerFont == null) {
        	headerFont = new TypeFont(new Font("Verdana", 10, Font.BOLD), 10);
        }

        return headerFont;
    }

    /**
     * A method to prevent the creation of too many TTF fonts causing
     * a memory leak. Returns the small font for the Client.
     *
     * @return smallFont
     *         The TTF for small fonts defined in a variable.
     */
    public TypeFont getSmallFont() {
        if (smallFont == null) {
        	smallFont = new TypeFont(new Font("Verdana", 6, Font.BOLD), 6);
        }

        return smallFont;
    }

    /**
	 * @return
	 * 		The current health of the player
	 */
	public float getHealth() {
		return Minecraft.getMinecraft().thePlayer.func_110143_aJ();
	}
	
	/**
	 * @return
	 * 		Instance of TextureManager, which is the new RenderEngine
	 */
	public TextureManager getRenderEngine() {
		return Minecraft.getMinecraft().func_110434_K();
	}
	
	/**
	 * Sets up a texture with a specified name and DynamicTexture.
	 * 
	 * @param name
	 * 		Name of the texture
	 * @param texture
	 * 		DynamicTexture to write to
	 * @return
	 * 		<code>func_110578_a(name, texture)</code>
	 */
	public ResourceLocation setupTexture(String name, DynamicTexture texture) {
		return this.getRenderEngine().func_110578_a(name, texture);
	}
	
	/**
	 * Binds a texture.
	 * 
	 * @param resourceLocation
	 * 		The ResourceLocation with the texture's information
	 */
	public void bindTexture(ResourceLocation resourceLocation) {
		this.getRenderEngine().func_110577_a(resourceLocation);
	}
	
	/**
	 * Gets the texture ID for a texture.
	 * 
	 * @param dynamicTexture
	 * 		The texture to get the ID for
	 * @return
	 * 		The ID of the DynamicTexture
	 */
	public int getTextureID(DynamicTexture dynamicTexture) {
		return dynamicTexture.func_110552_b();
	}
    
    /**
     * A method to pick out a specific hack from the ArrayList.
     *
     * @param name The name of the module that is trying to be located.
     * @return The module if there is one by the specified name, otherwise
     *         null.
     */
    public Module getModuleByName(String name) {
        for (Module module : Apolune.getInstance().getModuleManager().getModuleList()) {
            if (module.getName().equalsIgnoreCase(name)) {
                return module;
            }
        }
        return null;
    }

    /**
     * Used to get the coordinate that the text must be placed at
     * to be centered in a box with y coordinates y and y1.
     *
     * @param string   The string to calculate the height of.
     * @param y        The top of the box's y.
     * @param y1       The bottom of the box's y.
     * @param typeFont The font that the method should calculate for.
     * @return ((y + y1) / 2) - (typeFont.getStringHeight(string) / 2)
     *         The y coordinate the text needs to be placed at.
     */
    public float getCenteredY(String string, float y, float y1, TypeFont typeFont) {
        return ((int) (y + y1) / 2) - (typeFont.getStringHeight(string) / 2);
    }

    /**
     * This method adds a chat message to the players
     * chat box, client sided, with a special prefix
     * for the Apolune client.
     *
     * @param string;
     */
    public void addChat(String string) {
        Apolune.getInstance().getPlayer().addChatMessage("[" + getPrefix() + "Apolune\247f]: " + string);
    }

    /**
     * This method creates an Indented Rectangle on the screen.
     *
     * @param x      The x coordinate where the left edge of the box
     *               is to be aligned to. The top left of the screen is
     *               0, 0).
     * @param y      The y coordinate where the top edge of the box
     *               is to be aligned to. The top left of the screen is
     *               0, 0).
     * @param x1     The x coordinate where the right edge of the box
     *               is to be aligned to. The top left of the screen is
     *               0, 0).
     * @param y1     The y coordinate where the bottom edge of the box
     *               is to be aligned to. The top left of the screen is
     *               0, 0).
     * @param color  The hexadecimal color code that the light border will be
     *               drawn in by default.
     * @param color1 The hexadecimal color code that the dark border will be
     *               drawn in by default.
     */
    public void drawIndentedRect(int x, int y, int x1, int y1, int color, int color1) {
        Apolune.getInstance().getUtils().drawSideGradientRect(x, y, x + 3, y1 - 1, 0x00000000, 0x55000000);
        Apolune.getInstance().getUtils().drawRect(x, y, x + 0.5F, y1, color1);
        //Apolune.getInstance().getUtils().drawRect(x + 0.5F, y, x + 1, y1 - 1, color);

        Apolune.getInstance().getUtils().drawRect(x1 - 1, y + 0.5F, x1 - 0.5F, y1, color1);
        Apolune.getInstance().getUtils().drawRect(x1 - 0.5F, y, x1, y1, color);

        Apolune.getInstance().getUtils().drawGradientRect(x, y, x1, y + 4, 0x55000000, 0x00000000);
        Apolune.getInstance().getUtils().drawRect(x, y, x1, y + 0.5F, color1);
        //Apolune.getInstance().getUtils().drawRect(x + 1, y + 0.5F, x1 - 1, y + 1, color);

        Apolune.getInstance().getUtils().drawRect(x, y1 - 1, x1 - 0.5F, y1 - 0.5F, color1);
        Apolune.getInstance().getUtils().drawRect(x, y1 - 0.5F, x1, y1, color);
    }

    /**
     * This method creates a rectangle on the screen.
     *
     * @param x     The x coordinate where the left edge of the box
     *              is to be aligned to. The top left of the screen is
     *              0, 0).
     * @param y     The y coordinate where the top edge of the box
     *              is to be aligned to. The top left of the screen is
     *              0, 0).
     * @param x1    The x coordinate where the right edge of the box
     *              is to be aligned to. The top left of the screen is
     *              0, 0).
     * @param y1    The y coordinate where the bottom edge of the box
     *              is to be aligned to. The top left of the screen is
     *              0, 0).
     * @param color The hexadecimal color code that the rectangle will be
     *              drawn in by default.
     */
    public void drawRect(float x, float y, float x1, float y1, int color) {
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glEnable(GL11.GL_LINE_SMOOTH);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glPushMatrix();
        GL11.glColor4f(getRGBFromHex(color)[0], getRGBFromHex(color)[1],
                getRGBFromHex(color)[2], getRGBFromHex(color)[3]);
        GL11.glBegin(GL11.GL_QUADS);
        GL11.glVertex2d(x, y);
        GL11.glVertex2d(x, y1);
        GL11.glVertex2d(x1, y1);
        GL11.glVertex2d(x1, y);
        GL11.glEnd();
        GL11.glPopMatrix();
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_LINE_SMOOTH);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
    }

    /**
     * This method creates a Bordered Rectangle on the screen.
     *
     * @param x      The x coordinate where the left edge of the box
     *               is to be aligned to. The top left of the screen is
     *               0, 0).
     * @param y      The y coordinate where the top edge of the box
     *               is to be aligned to. The top left of the screen is
     *               0, 0).
     * @param x1     The x coordinate where the right edge of the box
     *               is to be aligned to. The top left of the screen is
     *               0, 0).
     * @param y1     The y coordinate where the bottom edge of the box
     *               is to be aligned to. The top left of the screen is
     *               0, 0).
     * @param l      The width of the border around the rectangle.
     * @param color  The hexadecimal color code that the rectangle will be
     *               drawn in by default.
     * @param color1 The hexadecimal color code that the border will be
     *               drawn in by default.
     */
    public void drawBorderedRect(float x, float y, float x1, float y1, float l, int color, int color1) {
        drawRect(x, y, x1, y1, color1);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glEnable(GL11.GL_LINE_SMOOTH);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glPushMatrix();
        GL11.glColor4f(getRGBFromHex(color)[0], getRGBFromHex(color)[1],
                getRGBFromHex(color)[2], getRGBFromHex(color)[3]);
        GL11.glLineWidth(l);
        GL11.glBegin(GL11.GL_LINES);
        GL11.glVertex2d(x, y);
        GL11.glVertex2d(x, y1);
        GL11.glVertex2d(x1, y1);
        GL11.glVertex2d(x1, y);
        GL11.glVertex2d(x, y);
        GL11.glVertex2d(x1, y);
        GL11.glVertex2d(x, y1);
        GL11.glVertex2d(x1, y1);
        GL11.glEnd();
        GL11.glPopMatrix();
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_LINE_SMOOTH);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
    }

    /**
     * This method draws a Bordered Gradient Rectangle on
     * the screen.
     *
     * @param x      The x coordinate where the left edge of the box
     *               is to be aligned to. The top left of the screen is
     *               0, 0).
     * @param y      The y coordinate where the top edge of the box
     *               is to be aligned to. The top left of the screen is
     *               0, 0).
     * @param x1     The x coordinate where the right edge of the box
     *               is to be aligned to. The top left of the screen is
     *               0, 0).
     * @param y1     The y coordinate where the bottom edge of the box
     *               is to be aligned to. The top left of the screen is
     *               0, 0).
     * @param l      The width of the border around the rectangle.
     * @param color  The hexadecimal color code that the border will be
     *               drawn in by default.
     * @param color1 The hexadecimal color code that the rectangle will be
     *               drawn in by initially.
     * @param color2 The hexadecimal color code that the rectangle will be
     *               fading into.
     */
    public void drawGradientBorderedRect(float x, float y, float x1, float y1, float l, int color, int color1, int color2) {
        drawGradientRect(x, y, x1, y1, color1, color2);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glEnable(GL11.GL_LINE_SMOOTH);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glPushMatrix();
        GL11.glColor4f(getRGBFromHex(color)[0], getRGBFromHex(color)[1],
                getRGBFromHex(color)[2], getRGBFromHex(color)[3]);
        GL11.glLineWidth(l);
        GL11.glBegin(GL11.GL_LINES);
        GL11.glVertex2d(x, y);
        GL11.glVertex2d(x, y1);
        GL11.glVertex2d(x1, y1);
        GL11.glVertex2d(x1, y);
        GL11.glVertex2d(x, y);
        GL11.glVertex2d(x1, y);
        GL11.glVertex2d(x, y1);
        GL11.glVertex2d(x1, y1);
        GL11.glEnd();
        GL11.glPopMatrix();
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_LINE_SMOOTH);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
    }

    /**
     * This method draws a Gradient Rectangle on the screen.
     *
     * @param x      The x coordinate where the left edge of the box
     *               is to be aligned to. The top left of the screen is
     *               0, 0).
     * @param y      The y coordinate where the top edge of the box
     *               is to be aligned to. The top left of the screen is
     *               0, 0).
     * @param x1     The x coordinate where the right edge of the box
     *               is to be aligned to. The top left of the screen is
     *               0, 0).
     * @param y1     The y coordinate where the bottom edge of the box
     *               is to be aligned to. The top left of the screen is
     *               0, 0).
     * @param color  The hexadecimal color code that the rectangle will be
     *               drawn in by initially.
     * @param color1 The hexadecimal color code that the rectangle will be
     *               fading into.
     */
    public void drawGradientRect(float x, float y, float x1, float y1, int color, int color1) {
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glEnable(GL11.GL_LINE_SMOOTH);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glShadeModel(GL11.GL_SMOOTH);
        GL11.glDisable(GL11.GL_ALPHA_TEST);
        GL11.glPushMatrix();
        GL11.glBegin(GL11.GL_QUADS);
        GL11.glColor4f(getRGBFromHex(color)[0], getRGBFromHex(color)[1],
                getRGBFromHex(color)[2], getRGBFromHex(color)[3]);
        GL11.glVertex2d(x1, y);
        GL11.glVertex2d(x, y);
        GL11.glColor4f(getRGBFromHex(color1)[0], getRGBFromHex(color1)[1],
                getRGBFromHex(color1)[2], getRGBFromHex(color1)[3]);
        GL11.glVertex2d(x, y1);
        GL11.glVertex2d(x1, y1);
        GL11.glEnd();
        GL11.glPopMatrix();
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_LINE_SMOOTH);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glShadeModel(GL11.GL_FLAT);
        GL11.glEnable(GL11.GL_ALPHA_TEST);
    }

    /**
     * This method draws a hollow box around the
     * selected coordinates.
     *
     * @param x     The x coordinate where the left edge of the box
     *              is to be aligned to. The top left of the screen is
     *              0, 0).
     * @param y     The y coordinate where the top edge of the box
     *              is to be aligned to. The top left of the screen is
     *              0, 0).
     * @param x1    The x coordinate where the right edge of the box
     *              is to be aligned to. The top left of the screen is
     *              0, 0).
     * @param y1    The y coordinate where the bottom edge of the box
     *              is to be aligned to. The top left of the screen is
     *              0, 0).
     * @param l     The width of the border.
     * @param color The hexadecimal color code that the border will be
     *              drawn in by default.
     */
    public void drawHollowBorderedRect(float x, float y, float x1, float y1, float l, int color) {
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glEnable(GL11.GL_LINE_SMOOTH);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glPushMatrix();
        GL11.glColor4f(getRGBFromHex(color)[0], getRGBFromHex(color)[1],
                getRGBFromHex(color)[2], getRGBFromHex(color)[3]);
        GL11.glLineWidth(l);
        GL11.glBegin(GL11.GL_LINES);
        GL11.glVertex2d(x, y);
        GL11.glVertex2d(x, y1);
        GL11.glVertex2d(x1, y1);
        GL11.glVertex2d(x1, y);
        GL11.glVertex2d(x, y);
        GL11.glVertex2d(x1, y);
        GL11.glVertex2d(x, y1);
        GL11.glVertex2d(x1, y1);
        GL11.glEnd();
        GL11.glPopMatrix();
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_LINE_SMOOTH);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
    }

    /**
     * This method draws a rectangle with a border
     * only on the left side and the right side of it.
     *
     * @param x      The x coordinate where the left edge of the box
     *               is to be aligned to. The top left of the screen is
     *               0, 0).
     * @param y      The y coordinate where the top edge of the box
     *               is to be aligned to. The top left of the screen is
     *               0, 0).
     * @param x1     The x coordinate where the right edge of the box
     *               is to be aligned to. The top left of the screen is
     *               0, 0).
     * @param y1     The y coordinate where the bottom edge of the box
     *               is to be aligned to. The top left of the screen is
     *               0, 0).
     * @param l      The width of the border around the rectangle.
     * @param color  The hexadecimal color code that the rectangle will be
     *               drawn in by default.
     * @param color1 The hexadecimal color code that the border will be
     *               drawn in by initially.
     */
    public void drawXBorderedRect(float x, float y, float x1, float y1, float l, int color, int color1) {
        drawRect(x + l, y, x1 - l, y1, color);
        drawRect(x, y, x - l, y1, color1);
        drawRect(x1 + l, y, x1, y1, color1);
    }

    /**
     * This method draws a Bordered Gradient Rectangle on
     * the screen.
     *
     * @param x      The x coordinate where the left edge of the box
     *               is to be aligned to. The top left of the screen is
     *               0, 0).
     * @param y      The y coordinate where the top edge of the box
     *               is to be aligned to. The top left of the screen is
     *               0, 0).
     * @param x1     The x coordinate where the right edge of the box
     *               is to be aligned to. The top left of the screen is
     *               0, 0).
     * @param y1     The y coordinate where the bottom edge of the box
     *               is to be aligned to. The top left of the screen is
     *               0, 0).
     * @param color1 The hexadecimal color code that the rectangle will be
     *               drawn in by initially.
     * @param color2 The hexadecimal color code that the rectangle will be
     *               fading into.
     */
    public void drawSideGradientRect(float x, float y, float x1, float y1, int color1, int color2) {
        float var7 = (float) (color2 >> 24 & 255) / 255.0F;
        float var8 = (float) (color2 >> 16 & 255) / 255.0F;
        float var9 = (float) (color2 >> 8 & 255) / 255.0F;
        float var10 = (float) (color2 & 255) / 255.0F;
        float var11 = (float) (color1 >> 24 & 255) / 255.0F;
        float var12 = (float) (color1 >> 16 & 255) / 255.0F;
        float var13 = (float) (color1 >> 8 & 255) / 255.0F;
        float var14 = (float) (color1 & 255) / 255.0F;
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_ALPHA_TEST);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glShadeModel(GL11.GL_SMOOTH);
        GL11.glBegin(GL11.GL_QUADS);
        GL11.glColor4f(var8, var9, var10, var7);
        GL11.glVertex3d((double) x, (double) y, (double) 0);
        GL11.glVertex3d((double) x, (double) y1, (double) 0);
        GL11.glColor4f(var12, var13, var14, var11);
        GL11.glVertex3d((double) x1, (double) y1, (double) 0);
        GL11.glVertex3d((double) x1, (double) y, (double) 0);
        GL11.glEnd();
        GL11.glShadeModel(GL11.GL_FLAT);
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glEnable(GL11.GL_ALPHA_TEST);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
    }

    /**
     * Enables and disables some GL constants
     * to prepare for rendering.
     */
    public void setupForRender() {
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glDisable(GL11.GL_LIGHTING);
        GL11.glDisable(2929);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glDepthMask(false);
    }

    /**
     * Enables and disabled some GL constants
     * after rendering has completed. To be
     * used after the above method. Does NOT
     * enable GL_LIGHTING.
     */
    public void finishRender() {
        GL11.glEnable(2929);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glDepthMask(true);
        GL11.glDisable(GL11.GL_BLEND);
    }

    /**
     * Draws a box around an AxisAlignedBB.
     *
     * @param axisAlignedBB The AxisAlignedBB to be drawn around
     */
    public void drawOutlinedBoundingBox(AxisAlignedBB axisAlignedBB) {
        Tessellator tess = Tessellator.instance;
        tess.startDrawing(3);
        tess.addVertex(axisAlignedBB.minX, axisAlignedBB.minY, axisAlignedBB.minZ);
        tess.addVertex(axisAlignedBB.maxX, axisAlignedBB.minY, axisAlignedBB.minZ);
        tess.addVertex(axisAlignedBB.maxX, axisAlignedBB.minY, axisAlignedBB.maxZ);
        tess.addVertex(axisAlignedBB.minX, axisAlignedBB.minY, axisAlignedBB.maxZ);
        tess.addVertex(axisAlignedBB.minX, axisAlignedBB.minY, axisAlignedBB.minZ);
        tess.draw();
        tess.startDrawing(3);
        tess.addVertex(axisAlignedBB.minX, axisAlignedBB.maxY, axisAlignedBB.minZ);
        tess.addVertex(axisAlignedBB.maxX, axisAlignedBB.maxY, axisAlignedBB.minZ);
        tess.addVertex(axisAlignedBB.maxX, axisAlignedBB.maxY, axisAlignedBB.maxZ);
        tess.addVertex(axisAlignedBB.minX, axisAlignedBB.maxY, axisAlignedBB.maxZ);
        tess.addVertex(axisAlignedBB.minX, axisAlignedBB.maxY, axisAlignedBB.minZ);
        tess.draw();
        tess.startDrawing(1);
        tess.addVertex(axisAlignedBB.minX, axisAlignedBB.minY, axisAlignedBB.minZ);
        tess.addVertex(axisAlignedBB.minX, axisAlignedBB.maxY, axisAlignedBB.minZ);
        tess.addVertex(axisAlignedBB.maxX, axisAlignedBB.minY, axisAlignedBB.minZ);
        tess.addVertex(axisAlignedBB.maxX, axisAlignedBB.maxY, axisAlignedBB.minZ);
        tess.addVertex(axisAlignedBB.maxX, axisAlignedBB.minY, axisAlignedBB.maxZ);
        tess.addVertex(axisAlignedBB.maxX, axisAlignedBB.maxY, axisAlignedBB.maxZ);
        tess.addVertex(axisAlignedBB.minX, axisAlignedBB.minY, axisAlignedBB.maxZ);
        tess.addVertex(axisAlignedBB.minX, axisAlignedBB.maxY, axisAlignedBB.maxZ);
        tess.draw();
    }

    /**
     * This is a method that removes colors
     * from a string so that friends can be
     * saved accurately.
     *
     * @param string The string that is to have the color
     *               codes replaced.
     * @return string
     *         The string after using .replace to remove
     *         all the color codes.
     */
    public String stripColorCodes(String string) {
        return this.colorCodes.matcher(string).replaceAll("");
    }
}
