package com.thepanthurr.apolune.managers;

import com.thepanthurr.apolune.event.EventHandler;
import com.thepanthurr.apolune.event.EventListener;
import com.thepanthurr.apolune.event.EventPriority;
import com.thepanthurr.apolune.event.events.EventChatSent;
import com.thepanthurr.apolune.event.events.EventGameStart;
import com.thepanthurr.apolune.module.Command;
import com.thepanthurr.apolune.module.commands.*;


import java.util.ArrayList;
import java.util.List;

/**
 * CommandManager is a class used for the batch handling
 * and dispatching of commands to Command Modules, as well as
 * holding an ArrayList of registered Command Modules.
 *
 * @author thePanthurr/ThatNahr
 * @version 1.0.5
 * @since 4/21/13
 */
public class CommandManager implements EventListener {

    private static volatile CommandManager newInstance;             //A variable for holding the single instance of CommandManager.
    private List<Command> commandList = new ArrayList<Command>();   //A variable for holding the current Array of Commands.

    /**
     * A Singleton method used to access the single instance of
     * CommandManager, it is double-locked and thread-safe.
     *
     * @return newInstance
     *         A variable of type CommandManager, only set to be other
     *         than null when this method is first called.
     */
    public static CommandManager getInstance() {
        if (newInstance == null) {
            synchronized (CommandManager.class) {
                if (newInstance == null) {
                    newInstance = new CommandManager();
                }
            }
        }
        return newInstance;
    }

    /**
     * This method runs when the game is loaded up
     * and simply adds all the Commands into an
     * ArrayList for further usage.
     *
     * @param eventGameStart An event parameter for EventHandler.
     */
    @EventHandler(priority = EventPriority.CRITICAL)
    public void onStart(EventGameStart eventGameStart) {
        commandList.add(new CommandFriend());
        commandList.add(new CommandHelp());
        commandList.add(new CommandKey());
        commandList.add(new CommandRainbow());
        commandList.add(new CommandSay());
        commandList.add(new CommandSet());
        commandList.add(new CommandVClip());
    }

    /**
     * Used for getting the commands currently
     * registered to the Client so that they can
     * be processed by Commands or I/O classes.
     *
     * @return commandList
     *         The ArrayList that Commands are
     *         registered to, at Runtime or when the
     *         user adds them in the game.
     */
    public List<Command> getCommandList() {
        return commandList;
    }

    /**
     * Used to handle the batch command running for the Command
     * Modules.
     *
     * @param eventChatSent An event parameter for EventHandler.
     */
    @EventHandler
    public void onChatSent(EventChatSent eventChatSent) {
        if (eventChatSent.getMessage().startsWith(".")) {
            for (Command command : commandList) {
                command.onCommand(eventChatSent.getMessage());
            }
            eventChatSent.setCancelled(true);
        }
    }
}
