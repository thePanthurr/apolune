package com.thepanthurr.apolune.managers;

import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.TypeFont.FontType;
import com.thepanthurr.apolune.event.EventHandler;
import com.thepanthurr.apolune.event.EventListener;
import com.thepanthurr.apolune.event.events.EventUpdate;
import com.thepanthurr.apolune.gui.Window;
import com.thepanthurr.apolune.module.Category;
import com.thepanthurr.apolune.module.Toggle;
import net.minecraft.src.GuiScreen;
import org.lwjgl.input.Keyboard;

import java.util.ArrayList;
import java.util.List;

/**
 * GuiManager is a class extending GuiScreen
 * that handles the batch drawing of the panels
 * to the screen.
 *
 * @author thePanthurr
 * @version 1.0.8
 * @since 4/28/13
 */
public class GuiManager extends GuiScreen implements EventListener {

    private static volatile GuiManager newInstance;             //A variable for holding the single instance of GuiManager.
    private List<Window> windowList = new ArrayList<Window>() {{ //A variable that holds a window for each category.
        int x = 2;

        for (Category category : Category.values()) {
            add(new Window(category, x, 2));
            x += 102;
        }
    }};

    /**
     * A Singleton method used to access the single instance of
     * GuiManager, it is double-locked and thread-safe.
     *
     * @return newInstance
     *         A variable of type GuiManager, only set to be other
     *         than null when this method is first called.
     */
    public static GuiManager getInstance() {
        if (newInstance == null) {
            synchronized (GuiManager.class) {
                if (newInstance == null) {
                    newInstance = new GuiManager();
                }
            }
        }
        return newInstance;
    }

    @Override
    public void drawScreen(int par1, int par2, float par3) {
        super.drawScreen(par1, par2, par3);

        for (Window window : windowList) {
            window.drawScreen(par1, par2, par3);
        }
    }

    @Override
    protected void keyTyped(char par1, int par2) {
        super.keyTyped(par1, par2);

        if (par2 == Keyboard.KEY_ESCAPE 
        		|| par2 == ((Toggle)Apolune.getInstance().getUtils().getModuleByName("GUI")).getKeyBind()) {
            Apolune.getInstance().getMinecraft().displayGuiScreen(null);
        }
    }

    @Override
    protected void mouseClicked(int par1, int par2, int par3) {
        super.mouseClicked(par1, par2, par3);

        for (int i = 0; i < windowList.size(); i++) {
            Window window = windowList.get(i);

            if (window.isMouseOver(par1, par2)) {
                for (Window window1 : windowList) {
                    if (window1 != window) {
                        window1.turnOffSliders();
                    }
                }
                if (i == windowList.size() - 1
                        || window.isMouseOverMinimize(par1, par2)
                        || window.isMouseOverPin(par1, par2)) {
                    Window curWindow = window;
                    windowList.remove(window);
                    windowList.add(curWindow);
                    window.mouseClicked(par1, par2, par3);
                    return;
                }

                Window curWindow = window;
                windowList.remove(window);
                windowList.add(curWindow);
            }
        }
    }

    @Override
    protected void mouseMovedOrUp(int par1, int par2, int par3) {
        super.mouseMovedOrUp(par1, par2, par3);

        for (Window window : windowList) {
            window.mouseMovedOrUp(par1, par2, par3);
        }
    }

    @Override
    public boolean doesGuiPauseGame() {
        return false;
    }

    /**
     * This method runs every render update and draws
     * the panels that are currently pinned, onto the
     * screen.
     *
     * @param eventUpdate An event parameter for EventHandler.
     */
    @EventHandler
    public void onUpdate(EventUpdate eventUpdate) {
        if (Apolune.getInstance().getMinecraft().currentScreen != newInstance) {
            for (Window window : windowList) {
                if (window.isPinned()) {
                    window.drawScreen(0, 0, 0);
                }
            }
        }
    }

    /**
     * A method used for Window clicking logic to make
     * sure that the window is handling which is on top.
     *
     * @return windowList
     *         An ArrayList of windows held in this class.
     */
    public List<Window> getWindowList() {
        return windowList;
    }
}
