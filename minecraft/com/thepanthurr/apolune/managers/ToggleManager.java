package com.thepanthurr.apolune.managers;

import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.TypeFont.FontType;
import com.thepanthurr.apolune.event.EventHandler;
import com.thepanthurr.apolune.event.EventListener;
import com.thepanthurr.apolune.event.EventPriority;
import com.thepanthurr.apolune.event.events.*;
import com.thepanthurr.apolune.module.Toggle;
import com.thepanthurr.apolune.module.toggles.*;
import net.minecraft.src.ScaledResolution;
import org.lwjgl.input.Keyboard;

import java.util.ArrayList;
import java.util.List;

/**
 * ToggleManager is a class used for the batch handling
 * and dispatching of commands to Toggle Modules, as well as
 * holding an ArrayList of registered Toggle Modules.
 *
 * @author thePanthurr/ThatNahr
 * @version 1.1.4
 * @since 4/14/13
 */
public class ToggleManager implements EventListener {

    private static volatile ToggleManager newInstance;          //A variable for holding the single instance of ToggleManager.
    private List<Toggle> toggleList = new ArrayList<Toggle>();  //A variable for holding the current Array of Toggles.

    /**
     * A Singleton method used to access the single instance of
     * ToggleManager, it is double-locked and thread-safe.
     *
     * @return newInstance
     *         A variable of type ToggleManager, only set to be other
     *         than null when this method is first called.
     */
    public static ToggleManager getInstance() {
        if (newInstance == null) {
            synchronized (ToggleManager.class) {
                if (newInstance == null) {
                    newInstance = new ToggleManager();
                }
            }
        }
        return newInstance;
    }

    /**
     * This method runs when the game is loaded up
     * and simply adds all the Toggles into an
     * ArrayList for further usage.
     *
     * @param eventGameStart An event parameter for EventHandler.
     */
    @EventHandler(priority = EventPriority.CRITICAL)
    public void onStart(EventGameStart eventGameStart) {
        addToggle(new ToggleAntiVelocity());
        addToggle(new ToggleBreadCrumbs());
        addToggle(new ToggleBrightness());
        addToggle(new ToggleESP());
        addToggle(new ToggleFade());
        addToggle(new ToggleFlight());
        addToggle(new ToggleGUI());
        addToggle(new ToggleKaiStep());
        addToggle(new ToggleKillAura());
        addToggle(new ToggleMarks());
        addToggle(new ToggleMobAura());
        addToggle(new ToggleNoCheat());
        addToggle(new ToggleNoFall());
        addToggle(new ToggleSoup());
        addToggle(new ToggleSpider());
        addToggle(new ToggleSprint());
        addToggle(new ToggleCriticals());
    }

    /**
     * Used for registering an event listener as well
     * as adding a Module at the same time.
     *
     * @param toggle The toggle to add to the ArrayList.
     */
    public void addToggle(Toggle toggle) {
        toggleList.add(toggle);
        Apolune.getInstance().getEventManager().registerListener(toggle);
    }

    /**
     * Used for getting the toggles currently
     * registered to the Client so that they can
     * be processed by Commands or I/O classes.
     *
     * @return toggleList
     *         The ArrayList that Toggles are
     *         registered to, at Runtime or when the
     *         user adds them in the game.
     */
    public List<Toggle> getToggleList() {
        return toggleList;
    }

    /**
     * Used to handle the batch keybindings for the Toggle
     * Modules.
     *
     * @param eventKeyPressed An event parameter for EventHandler.
     */
    @EventHandler
    public void onKeyPressed(EventKeyPressed eventKeyPressed) {
        for (Toggle toggle : toggleList) {
            if (toggle.getKeyBind() == eventKeyPressed.getKey()
                    && toggle.getKeyBind() != Keyboard.KEY_NONE) {
                toggle.toggle();
            }
        }
    }

    /**
     * Used to handle the batch drawing for the Toggle
     * Modules names to the screen.
     *
     * @param eventUpdate An event parameter for EventHandler.
     */
    @EventHandler
    public void onUpdate(EventUpdate eventUpdate) {
        int ypos = 2;
        ScaledResolution scaledResolution = new ScaledResolution(Apolune.getInstance().getGameSettings(),
                Apolune.getInstance().getMinecraft().displayWidth,
                Apolune.getInstance().getMinecraft().displayHeight);
        for (Toggle toggle : toggleList) {
            float maxMultiplier = 50f;      //Used for changing how fast the text fades. Smaller number = faster.
            int delta = 2;                  //Also used for changing the speed of the fading. Bigger number = faster.
            if (toggle.isToggled() && toggle.getToggleTime() < maxMultiplier) {
                toggle.setToggleTime(delta);
            } else if (toggle.getToggleTime() > 0) {
                toggle.setToggleTime(-delta);
            }
            int alpha = Math.round(255f * ((float) toggle.getToggleTime() / maxMultiplier));
            if (alpha > 0) {
                Apolune.getInstance().getUtils().getDefaultFont().drawString(toggle.getName(),
                        scaledResolution.getScaledWidth()
                        - Apolune.getInstance().getUtils().getDefaultFont().getStringWidth(toggle.getName()) - 2,
                        ypos, FontType.SHADOW_THICK, toggle.getCategory().getColor(), 0xFF000000);
                ypos += 10;
            }
        }
    }

}
