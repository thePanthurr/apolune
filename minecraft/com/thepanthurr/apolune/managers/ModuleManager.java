package com.thepanthurr.apolune.managers;

import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.event.EventHandler;
import com.thepanthurr.apolune.event.EventListener;
import com.thepanthurr.apolune.event.EventPriority;
import com.thepanthurr.apolune.event.events.EventGameStart;
import com.thepanthurr.apolune.module.Command;
import com.thepanthurr.apolune.module.Module;
import com.thepanthurr.apolune.module.Toggle;

import java.util.ArrayList;
import java.util.List;

/**
 * ModuleManager is a Manager used to hold an ArrayList
 * of all registered Modules. As well as later controlling
 * which Modules are in the client.
 *
 * @author thePanthurr
 * @version 1.0.0
 * @since 4/23/13
 */
public class ModuleManager implements EventListener {

    private static volatile ModuleManager newInstance; //A variable for holding the single instance of ModuleManager.
    private List<Module> moduleList = new ArrayList<Module>();

    /**
     * A Singleton method used to access the single instance of
     * ModuleManager, it is double-locked and thread-safe.
     *
     * @return newInstance
     *         A variable of type ModuleManager, only set to be other
     *         than null when this method is first called.
     */
    public static ModuleManager getInstance() {
        if (newInstance == null) {
            synchronized (ModuleManager.class) {
                if (newInstance == null) {
                    newInstance = new ModuleManager();
                }
            }
        }
        return newInstance;
    }

    /**
     * This method is ran on startup and simply caches the
     * Toggles and Commands into another ArrayList of Modules.
     *
     * @param eventGameStart An event parameter for EventHandler.
     */
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onStart(EventGameStart eventGameStart) {
        for (Toggle toggle : Apolune.getInstance().getToggleManager().getToggleList()) {
            moduleList.add(toggle);
        }

        for (Command command : Apolune.getInstance().getCommandManager().getCommandList()) {
            moduleList.add(command);
        }
    }

    /**
     * Used for commands and things that need to
     * access Modules in general instead of just toggles
     * such as Commands.
     *
     * @return moduleList
     *         The ArrayList of modules that the Toggles and
     *         Commands are added to.
     */
    public List<Module> getModuleList() {
        return moduleList;
    }
}
