package com.thepanthurr.apolune.gui;

/**
 * Element is an interface used to aid in the creation of
 * Gui objects such as windows, buttons, etc.
 *
 * @author thePanthurr
 * @version 1.0.3
 * @since 4/28/13
 */
public interface Element {

    int colorLight = 0xff333333;    //A variable to hold the light color of the window, to keep it consistent.
    int colorDark = 0xff111111;     //A variable to hold the dark color of the window, to keep it consistent.

    /**
     * A method for displaying the element onto the screen,
     * called in GuiManager.
     *
     * @param i The x coordinate of the mouse.
     * @param j The y coordinate of the mouse.
     * @param f The number of ticks it is taking to draw.
     */
    public void drawScreen(int i, int j, float f);

    /**
     * A method for handling a click while the GuiManager is
     * open.
     *
     * @param i The x coordinate of the mouse.
     * @param j The y coordinate of the mouse.
     * @param k The button pressed, 1 is left click
     *          2 is right click.
     */
    public void mouseClicked(int i, int j, int k);

    /**
     * A method for handling a release while the GuiManager is
     * open.
     *
     * @param i The x coordinate of the mouse.
     * @param j The y coordinate of the mouse.
     * @param k The button pressed, 0 is left click
     *          1 is right click.
     */
    public void mouseMovedOrUp(int i, int j, int k);


    /**
     * A method used to determine if the mouse is over the element,
     * makes sure that clicks are stacking.
     *
     * @param i The x coordinate of the mouse.
     * @param j The y coordinate of the mouse.
     * @return true if the mouse is over the element
     *         || false if the mouse is not.
     */
    public boolean isMouseOver(int i, int j);

    /**
     * A method used to handle dragging.
     *
     * @param i The amount of x that is being shifted.
     * @param j The amount of y that is being shifted.
     */
    public void shiftPosition(int i, int j);
}
