package com.thepanthurr.apolune.gui;

import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.TypeFont.FontType;
import com.thepanthurr.apolune.module.Toggle;
import com.thepanthurr.apolune.module.Value;

import java.text.DecimalFormat;

/**
 * Slider is a GUI element for modifying the
 * value of a Module's Value.
 *
 * @author ThatNahr/thePanthurr
 * @version 1.0.1
 * @since 6/9/13
 */
public class Slider implements Element {

    private Value value;            // Variable for holding the Value to modify
    private float maxValue;        // Variable for holding the maximum value of the Value
    private int x;                    // Variable for holding the x coordinate of the Slider
    private int y;                    // Variable for holding the y coordinate of the Slider
    private int width;                // Variable used for holding the calculated width of the Slider
    private int height;                // Variable used for holding the calculated height of the Slider
    private boolean dragging;        // Variable used for when the Slider is being dragged
    private float percentage;        // Variable used for determining the current location of the Slider, as well as the return Value

    public Slider(Value value, int x, int y, int x1, int y1) {
        this.value = value;
        this.x = x;
        this.y = y;
        this.width = x1 - x;
        this.height = y1 - y;
        this.maxValue = value.getFloat()[1];
        this.percentage = value.getFloat()[0] / this.maxValue;
    }

    @Override
    public void drawScreen(int i, int j, float f) {
        this.maxValue = value.getFloat()[((Toggle) Apolune.getInstance().getUtils().getModuleByName("NoCheat")).isToggled() ? 3 : 1];
        mouseDragged(i, j);
        DecimalFormat format = new DecimalFormat("#.##");
        float drawToPercent;

        if (((Toggle) Apolune.getInstance().getUtils().getModuleByName("NoCheat")).isToggled()
                && value.getFloat()[0] == value.getFloat()[3]) {
            drawToPercent = this.width - 1;
        } else {
            drawToPercent = this.percentage * (this.width - 1);
        }

        // Background gradient
        Apolune.getInstance().getUtils().drawGradientRect(this.x, this.y, this.x + this.width, this.y + this.height,
                0xff3B3B3B, 0xff262626);

        Apolune.getInstance().getUtils().drawIndentedRect(this.x, this.y, this.x + this.width, this.y + this.height,
                0x70909090, this.colorDark);

        // Gradient rectangle representing the value
        Apolune.getInstance().getUtils().drawGradientRect(this.x + 1, this.y + 1f, this.x + drawToPercent, this.y + this.height - 1f,
                Apolune.getInstance().getUtils().getTintedColor(Apolune.getInstance().getUtils().getColor(), 115),
                Apolune.getInstance().getUtils().getTintedColor(Apolune.getInstance().getUtils().getColor(), 75));

        Apolune.getInstance().getUtils().getSmallFont()
                .drawString(Apolune.getInstance().getUtils().capitalize(value.getKey() + ":"), x + 2, (y + y + height) / 2
                        - Apolune.getInstance().getUtils().getSmallFont().getStringHeight(value.getKey() + ":") / 2 + 2,
                        FontType.SHADOW_THICK, 0xffffffff, 0xFF000000);

        Apolune.getInstance().getUtils().getSmallFont()
                .drawString(Apolune.getInstance().getUtils().capitalize(format.format(value.getFloat()[0])), x + width - 2
                        - Apolune.getInstance().getUtils().getSmallFont().getStringWidth(format.format(value.getFloat()[0])), (y + y + height) / 2
                        - Apolune.getInstance().getUtils().getSmallFont().getStringHeight(value.getKey() + ":") / 2 + 2,
                        FontType.SHADOW_THICK, 0xffffffff, 0xFF000000);
    }

    @Override
    public void mouseClicked(int i, int j, int k) {
        if (this.isMouseOver(i, j)
                && k == 0) {
            this.percentage = (float) (i - this.x) / (this.width);

            if (this.percentage < 0.0F) {
                this.percentage = 0.0F;
            }

            if (this.percentage > 1.0F) {
                this.percentage = 1.0F;
            }

            this.value.setInitial((this.maxValue - value.getFloat()[3]) * this.percentage + value.getFloat()[3]);
            this.dragging = true;
        } else {
            this.dragging = false;
        }
    }

    /**
     * Method used to handling situations when the mouse
     * is dragged from one point to another.
     *
     * @param i The x coordinate of the mouse.
     * @param j The y coordinate of the mouse.
     */
    public void mouseDragged(int i, int j) {
        if (this.dragging) {
            this.percentage = (float) (i - this.x) / (this.width);

            if (this.percentage < 0.0F) {
                this.percentage = 0.0F;
            }

            if (this.percentage > 1.0F) {
                this.percentage = 1.0F;
            }

            this.value.setInitial((this.maxValue - value.getFloat()[2]) * this.percentage + value.getFloat()[2]);
        }
    }

    @Override
    public void mouseMovedOrUp(int i, int j, int k) {
        this.dragging = false;
        this.value.setInitial((this.maxValue - value.getFloat()[2]) * this.percentage + value.getFloat()[2]);
        Apolune.getInstance().getSaveValues().save();
    }

    @Override
    public boolean isMouseOver(int i, int j) {
        return (i >= this.x)
                && (i <= this.x + this.width)
                && (j >= this.y)
                && (j <= this.y + this.height);
    }

    @Override
    public void shiftPosition(int i, int j) {
        x = i;
        y = j;
    }

}
