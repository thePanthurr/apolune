package com.thepanthurr.apolune.gui;

import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.TypeFont.FontType;
import com.thepanthurr.apolune.module.Toggle;
import com.thepanthurr.apolune.module.Value;

import java.util.ArrayList;
import java.util.List;

/**
 * Button is a simple click-to-toggle functionality
 * created in order to turn Toggle Modules on and off.
 *
 * @author thePanthurr
 * @version 1.0.8
 * @since 4/28/13
 */
public class Button implements Element {

    private Toggle toggle;                                      //A variable for holding the toggle that the Button interacts with.
    private int x;                                              //A variable for holding the x position of the Button.
    private int y;                                              //A variable for holding the y position of the Button.
    private int x1;                                             //A variable for holding the ending x coordinate of the Button.
    private int y1;                                             //A variable for holding the ending y coordinate of the Button.
    private int width;                                          //A variable to hold the calculated width of the Button.
    private int height;                                         //A variable to hold the calculated height of the Button.
    private boolean pressed;                                    //A variable used to hold whether or not the button is being held down.
    private boolean top;                                        //A variable used to hold if the parent window is on the top or not.
    private List<Slider> sliderList = new ArrayList<Slider>();  //A list to cache all sliders for the specified toggle.
    private boolean displaySlider = false;                      //A variable to hold whether or not the button is to be shown instead of the slider.
    private int currentIndex = 0;                               //A variable to hold which slider in the Array is being displayed.

    public Button(Toggle toggle, int x, int y, int x1, int y1) {
        this.toggle = toggle;
        this.x = x;
        this.y = y;
        this.x1 = x1;
        this.y1 = y1;
        this.width = x1 - x;
        this.height = y1 - y;
        loadSliders();
    }

    @Override
    public void drawScreen(int i, int j, float f) {
        Apolune.getInstance().getUtils().drawIndentedRect(x, y, x1, y1, 0x70909090, colorDark);
        if (!displaySlider) {
            int tempColor = 0x00000000;
            int tempColor2 = 0x00000000;

            if (toggle.isToggled()) {
                tempColor = Apolune.getInstance().getUtils().getColor();
                if (pressed) {
                    tempColor = Apolune.getInstance().getUtils().getTintedColor(Apolune.getInstance().getUtils().getColor(), 75);
                    tempColor2 = Apolune.getInstance().getUtils().getTintedColor(Apolune.getInstance().getUtils().getColor(), 50);
                } else {
                    tempColor2 = Apolune.getInstance().getUtils().getTintedColor(Apolune.getInstance().getUtils().getColor(), 75);
                    if (isMouseOver(i, j)) {
                        tempColor = Apolune.getInstance().getUtils().getTintedColor(Apolune.getInstance().getUtils().getColor(), 115);
                        tempColor2 = Apolune.getInstance().getUtils().getTintedColor(Apolune.getInstance().getUtils().getColor(), 90);
                    }
                }
            } else {
                if (pressed) {
                    tempColor = 0x60000000;
                    tempColor2 = 0x60000000;
                } else {
                    if (!isMouseOver(i, j)) {
                        tempColor = 0x00000000;
                        tempColor2 = 0x00000000;
                    } else {
                        tempColor = 0x10cccccc;
                        tempColor2 = 0x10cccccc;
                    }
                }
            }

            Apolune.getInstance().getUtils().drawGradientRect(x + 0.5F, y + 0.5F, x1 - 0.5F, y1 - 0.5F,
                    tempColor, tempColor2);

            Apolune.getInstance().getUtils().getDefaultFont().drawString(toggle.getName(),
                    ((x + x1) / 2) - (Apolune.getInstance().getUtils().getDefaultFont().getStringWidth(toggle.getName()) / 2),
                    y + 5, FontType.SHADOW_THICK, top ? 0xffffffff : 0xffaaaaaa, 0xFF000000);
        } else {
            sliderList.get(currentIndex).drawScreen(i, j, f);
        }
    }

    @Override
    public void mouseClicked(int i, int j, int k) {
        if (isMouseOver(i, j)) {
            if (displaySlider) {
                if (k == 1) {
                    if (currentIndex == sliderList.size() - 1) {
                        displaySlider = false;
                        currentIndex = 0;
                    } else {
                        currentIndex += 1;
                    }
                }
            } else {
                switch (k) {
                    case 0:
                        if (!displaySlider) {
                            pressed = true;
                        }
                        break;
                    case 1:
                        if (!sliderList.isEmpty()) {
                            displaySlider = true;
                        }
                }
            }

            for (Slider slider : sliderList) {
                slider.mouseClicked(i, j, k);
            }
        } else {
            displaySlider = false;
        }
    }

    @Override
    public void mouseMovedOrUp(int i, int j, int k) {
        if (isMouseOver(i, j) && pressed) {
            if (k == 0) {
                toggle.toggle();
            }
        }
        pressed = false;

        for (Slider slider : sliderList) {
            slider.mouseMovedOrUp(i, j, k);
        }
    }

    @Override
    public boolean isMouseOver(int i, int j) {
        return ((i >= x)
                && (i <= x1)
                && (j >= y)
                && (j <= y1));
    }

    @Override
    public void shiftPosition(int i, int j) {
        x = i + 2;
        x1 = x + width;
        y = j;
        y1 = y + height;

        for (Slider slider : sliderList) {
            slider.shiftPosition(x, y);
        }
    }

    /**
     * A method called when the button is initialized,
     * creates sliders for the toggle so that they can be
     * cycled through on click.
     */
    private void loadSliders() {
        for (Value value : toggle.getValues()) {
            if (!value.getStrings()[0].equalsIgnoreCase("true")
                    && !value.getStrings()[0].equalsIgnoreCase("false")) {
                sliderList.add(new Slider(value, x, y, x1, y1));
            }
        }
    }

    /**
     * Used to make sure that the buttons hover effects
     * aren't used if the window is not on top.
     *
     * @param top The variable that controls the rendering of
     *            the hover effect.
     */
    public void setTop(boolean top) {
        this.top = top;
    }

    /**
     * A method to cancel sliders when another window is clicked.
     *
     * @param displaySlider The boolean value that will decide whether or not a slider
     *                      is displayed with drawScreen is called.
     */
    public void setDisplaySlider(boolean displaySlider) {
        this.displaySlider = displaySlider;
    }
}
