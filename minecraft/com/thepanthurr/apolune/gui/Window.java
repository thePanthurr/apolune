package com.thepanthurr.apolune.gui;

import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.TypeFont.FontType;
import com.thepanthurr.apolune.module.Category;
import com.thepanthurr.apolune.module.Toggle;

import java.util.ArrayList;
import java.util.List;


/**
 * Window is an object that handles the movement,
 * drawing, and creation of a rectangular panel containing
 * hacks.
 *
 * @author thePanthurr/ThatNahr
 * @version 1.1.2
 * @since 4/28/13
 */
public class Window implements Element {

    private Category category;                                  //A variable to hold the Category that the title will represent, and the hacks under it.
    private int x;                                              //A variable to hold the x coordinate that the left of the window will be placed at.
    private int y;                                              //A variable to hold the y coordinate that the top of the window will be placed at.
    private int y1;                                             //A variable to hold the y1 coordinate that the bottom of the window will be placed at.
    private int offx;                                           //A variable that holds the x offset from when the mouse is first clicked.
    private int offy;                                           //A variable that holds the y offset from when the mouse is first clicked.
    private int width = 100;                                    //A predefined variable holding the width of each Window.
    private int height;                                         //The height of the window, is the very bottom of all the buttons.
    private boolean dragged;                                    //A variable to hold whether or not the window is attempting to be dragged.
    private boolean pinned;                                     //A variable to hold if the window should be displayed when the Gui is closed.
    private boolean minimized;                                  //A variable to hold whether or not the windows contents are to be displayed.
    private boolean top;                                        //A variable to hold whether or not this Window object is the "top" window.
    private long click = -1;                                    //A variable to hold the amount of time since the header has been clicked.
    private List<Button> buttonList = new ArrayList<Button>();  //A list of buttons that contain modules with the same category as the window.

    public Window(Category category, int x, int y) {
        this.category = category;
        this.x = x;
        this.y = y;
        loadButtons();
    }

    @Override
    public void drawScreen(int i, int j, float f) {
        this.top = Apolune.getInstance().getGuiManager().getWindowList().indexOf(this) ==
                Apolune.getInstance().getGuiManager().getWindowList().size() - 1;

        if (dragged) {
            shiftPosition(i - offx, j - offy);
        }

        Apolune.getInstance().getUtils().drawGradientBorderedRect(x, y, x + width, !minimized ? y1 : y + 14, 0.5F,
                0xff000000, colorLight, colorLight);

        Apolune.getInstance().getUtils().drawGradientRect(x + 1, y + 1, x + width - 1, y + 11, 0x40808080, 0);

        Apolune.getInstance().getUtils().drawRect(x + width - 13.5f, y + 0.5f, x + width - 13, y + 13.5f, colorDark);
        Apolune.getInstance().getUtils().drawRect(x + width - 13, y + 0.5f, x + width - 12.5f, y + 13.5f, 0x70777777);

        Apolune.getInstance().getUtils().drawRect(x + width - 26.5f, y + 0.5f, x + width - 26f, y + 13.5f, colorDark);
        Apolune.getInstance().getUtils().drawRect(x + width - 26, y + 0.5f, x + width - 25.5f, y + 13.5f, 0x70777777);

        if (pinned) {
            Apolune.getInstance().getUtils().drawRect(x + width - 13f, y + 0.5f, x + width - 0.5f, y + 13.5f, 0x70000000);
        }

        Apolune.getInstance().getUtils().getHeaderFont().drawString(category.getName(),
                x + 2, y + 2, FontType.SHADOW_THICK, this.top ? 0xffffffff : 0xffaaaaaa, 0xFF000000);


        if (!minimized) {
            for (Button button : buttonList) {
                button.setTop(this.top);
                button.drawScreen(i, j, f);
            }
        } else {
            Apolune.getInstance().getUtils().drawRect(x + width - 26, y + 0.5f, x + width - 13.5f, y + 13.5f, 0x70000000);
        }

        if (isMouseOverMinimize(i, j)) {
            Apolune.getInstance().getUtils().getSmallFont().drawString("_", x + width - 22, y + 6, FontType.NORMAL, this.top && !this.minimized ? 0xFFCCCCCC : 0xFFAAAAAA, 0xFF000000);
        }

        if (isMouseOverPin(i, j)) {
            Apolune.getInstance().getUtils().getSmallFont().drawString("*", x + width - 9, y + 8, FontType.NORMAL, this.top && !this.pinned ? 0xFFCCCCCC : 0xFFAAAAAA, 0xFF000000);
        }
    }

    @Override
    public void mouseClicked(int i, int j, int k) {
        if (isMouseOverPin(i, j)) {
            pinned = !pinned;
            return;
        }

        if (isMouseOverMinimize(i, j)) {
            minimized = !minimized;
            return;
        }

        if (isMouseOverTitle(i, j)) {
            if (System.nanoTime() / 1000000 - click <= 250) {
                minimized = !minimized;
            } else {
                click = System.nanoTime() / 1000000;
            }

            if ((i >= x) && (i <= x + width) && (j >= y) && (j <= y + 14)) {
                dragged = true;
                offx = i - x;
                offy = j - y;
            }
        }

        for (Button button : buttonList) {
            button.mouseClicked(i, j, k);
        }
    }

    @Override
    public void mouseMovedOrUp(int i, int j, int k) {
        dragged = false;

        for (Button button : buttonList) {
            button.mouseMovedOrUp(i, j, k);
        }
    }

    @Override
    public boolean isMouseOver(int i, int j) {
        boolean over = minimized
                ? isMouseOverTitle(i, j)
                : (i >= x) && (i <= x + width) && (j >= y) && (j <= y1);
        boolean flag = false;
        if (over) {
            for (Window window : Apolune.getInstance().getGuiManager().getWindowList()) {
                if (window != this
                        && Apolune.getInstance().getGuiManager().getWindowList().indexOf(window) >
                        Apolune.getInstance().getGuiManager().getWindowList().indexOf(this)
                        && window.isMouseOver(i, j)) {
                    return false;
                }
            }
        }

        return over;
    }

    /**
     * Used to determine if the mouse was hovering
     * over the area to toggle minimization.
     *
     * @param i The x coordinate of the mouse.
     * @param j The y coordinate of the mouse.
     * @return If the mouse coordinates are inside
     *         the minimization button coordinates.
     */
    public boolean isMouseOverMinimize(int i, int j) {
        return ((i >= x + width - 26) && (i <= x + width - 14f) && (j >= y) && (j <= y + 13));
    }

    /**
     * Used to determine if the mouse was hovering
     * over the area to toggle pinning.
     *
     * @param i The x coordinate of the mouse.
     * @param j The y coordinate of the mouse.
     * @return If the mouse coordinates are inside
     *         the pin button coordinates.
     */
    public boolean isMouseOverPin(int i, int j) {
        return ((i >= x + width - 13) && (i <= x + width) && (j >= y) && (j <= y + 13));
    }

    /**
     * Used to determine if the mouse was clicked right
     * over the top bar, to handle dragging.
     *
     * @param i The x coordinate of the mouse.
     * @param j The y coordinate of the mouse.
     * @return If the mouse coordinates are inside the
     *         title bar coordinates.
     */
    public boolean isMouseOverTitle(int i, int j) {
        return ((i >= x) && (i <= x + width) && (j >= y) && (j <= y + 14));
    }

    @Override
    public void shiftPosition(int i, int j) {
        x = i;
        y = j;
        y1 = y + height;

        for (int i1 = 0; i1 < buttonList.size(); i1++) {
            Button button = buttonList.get(i1);
            button.shiftPosition(x, y + 14 + (18 * i1));
        }
    }

    /**
     * Loops through each Toggle Module, and if the category
     * is the same as the window's, it adds the button to the
     * Window's array.
     */
    public void loadButtons() {
        buttonList.clear();
        height = y + 12;
        if (!minimized) {
            for (Toggle toggle : Apolune.getInstance().getToggleManager().getToggleList()) {
                if (toggle.getCategory() == category
                        && !toggle.getDescription().contains("A keybind")) {
                    height += 18;
                    buttonList.add(new Button(toggle, x + 2, height - 16, x + width - 2, height));
                }
            }
        }

        y1 = height + 2;
    }

    /**
     * Used in GuiManager in order to determine
     * which windows should be drawn to the screen
     * when the gui screen is closed.
     *
     * @return pinned
     *         The variable that is changed by clicking
     *         the pin button.
     */
    public boolean isPinned() {
        return pinned;
    }

    /**
     * A method called in GuiManager that simply stops
     * displaying sliders in the window.
     */
    public void turnOffSliders() {
        for (Button button : buttonList) {
            button.setDisplaySlider(false);
        }
    }
}
