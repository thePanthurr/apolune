package com.thepanthurr.apolune.event.events;

import com.thepanthurr.apolune.event.Event;

/**
 * EventKeyPressed is called in runTick() in Minecraft.java.
 * It records the key pressed as well.
 *
 * @author thePanthurr
 * @version 1.0.0
 * @since 4/17/13
 */
public class EventKeyPressed extends Event {

    private int key; //A variable holding the key that was pressed.

    public EventKeyPressed(int key) {
        this.key = key;
    }

    /**
     * Returns the key pressed when the event was fired.
     *
     * @return key
     *         The integer key code of the pressed key.
     */
    public int getKey() {
        return key;
    }
}
