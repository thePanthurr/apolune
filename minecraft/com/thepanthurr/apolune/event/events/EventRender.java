package com.thepanthurr.apolune.event.events;

import com.thepanthurr.apolune.event.Event;

/**
 * EventRender is called in EntityRenderer.
 * Obviously, it is used for any modules
 * that need to have something render * on-screen, or when they need to update
 * faster than other events.
 *
 * @author ThatNahr
 * @version 1.0.0
 * @since 4/24/13
 */
public class EventRender extends Event {

}
