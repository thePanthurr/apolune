package com.thepanthurr.apolune.event.events;

import com.thepanthurr.apolune.event.Event;

/**
 * EventChatSent is called whenever the player types a message
 * into the chat. It allows the message to be passed to any
 * class referencing the Event. It is called in EntityClientPlayerMP
 * in the sendChatMessage method.
 *
 * @author thePanthurr
 * @version 1.0.1
 * @since 4/21/13
 */
public class EventChatSent extends Event {

    private String message; //A variable used to hold the current message being sent through the chat.

    public EventChatSent(String message) {
        this.message = message;
    }

    /**
     * Allows the message being sent to be passed to other classes
     * such as the CommandManager.
     *
     * @return message
     *         The message that was typed into the chat.
     */
    public String getMessage() {
        return message;
    }
}
