package com.thepanthurr.apolune.event.events;

/**
 * EventAttackEntity is called in PlayerControllerMP under
 * the method "attackEntity".  It can be used for any modules
 * that have something to do when an Entity is attacked.
 *
 * @author Knar.
 * @version 1.0.1
 * @since 7/24/13
 */

import net.minecraft.src.Entity;
import net.minecraft.src.EntityPlayer;
import com.thepanthurr.apolune.event.Event;

public class EventAttackEntity extends Event{

	private EntityPlayer eplayer;
	private Entity entity;
	
	public EventAttackEntity(EntityPlayer par1EntityPlayer, Entity par2Entity) {
		this.eplayer = par1EntityPlayer;
		this.entity = par2Entity;
	}
	
	public EntityPlayer getEntityPlayer() {
		return this.eplayer;
	}
	
	public Entity getEntity() {
		return this.entity;
	}
	
}
