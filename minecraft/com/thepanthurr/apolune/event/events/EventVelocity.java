package com.thepanthurr.apolune.event.events;

import net.minecraft.src.Entity;

import com.thepanthurr.apolune.event.Event;

/**
 * EventVelocity is called in NetClientHandler's
 * method, "handleEntityVelocity," as well as in
 * BlockFluid's method, "velocityToAddToEntity."
 * The only use is to cancel the effects made on
 * the player by these methods.
 *
 * @author ThatNahr
 * @version 1.0.1
 * @since 5/7/13
 */
public class EventVelocity extends Event {

    private Entity affectedEntity; //Variable used to hold the Entity that is affected by handleEntityVelocity

    public EventVelocity(Entity affectedEntity) {
        this.affectedEntity = affectedEntity;
    }

    /**
     * Returns the affected Entity from handleEntityVelocity
     *
     * @return affectedEntity
     *         The Entity affected by handleEntityVelocity
     */
    public Entity getAffectedEntity() {
        return this.affectedEntity;
    }

}
