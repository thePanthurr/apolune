package com.thepanthurr.apolune.event.events;

import com.thepanthurr.apolune.event.Event;

/**
 * EventPreUpdate is called in EntityClientPlayerMP and is run
 * before the game sends a motion update.
 *
 * @author thePanthurr
 * @version 1.0.0
 * @since 4/22/13
 */
public class EventPreUpdate extends Event {

}
