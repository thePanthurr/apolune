package com.thepanthurr.apolune.event.events;

import com.thepanthurr.apolune.event.Event;

/**
 * EventGameStart is called in Minecraft.java in the startGame
 * method, and is only run once.
 *
 * @author thePanthurr
 * @version 1.0.0
 * @since 4/17/13
 */
public class EventGameStart extends Event {

}
