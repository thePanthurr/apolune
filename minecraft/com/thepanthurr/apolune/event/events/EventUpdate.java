package com.thepanthurr.apolune.event.events;

import com.thepanthurr.apolune.event.Event;

/**
 * EventUpdate is called in GuiIngame and is called whenever
 * the game's overlay is rendered. It is used in most updating
 * modules.
 *
 * @author thePanthurr
 * @version 1.0.0
 * @since 4/15/13
 */
public class EventUpdate extends Event {

}
