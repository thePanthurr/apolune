package com.thepanthurr.apolune.event.events;

import net.minecraft.src.Packet;

import com.thepanthurr.apolune.event.Event;

public class EventReceivePacket extends Event {

	Packet packetReceived;
	
	public EventReceivePacket(Packet p) {
		this.packetReceived = p;
	}
	
	public Packet getPacket() {
		return this.packetReceived;
	}
	
}
