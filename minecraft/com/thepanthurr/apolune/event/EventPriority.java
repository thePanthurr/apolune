package com.thepanthurr.apolune.event;

/**
 * EventPriority is an enum used to keep track of the method
 * priority to be run when an event is called, it has 6 levels
 * of priority, Lowest, Lower, Default, Higher, Highest, and Critical.
 *
 * @author thePanthurr
 * @version 1.0.1
 * @since 4/14/13
 */
public enum EventPriority {

    /**
     * This priority is only used for loading, highest priority.
     */
    CRITICAL,

    /**
     * This priority is only used for low-importance client functions.
     */
    HIGHEST,

    /**
     * The priority used for important Module functions.
     */
    HIGHER,

    /**
     * The default priority for methods ran.
     */
    DEFAULT,

    /**
     * The priority lower than normal, and higher than lowest, used to proceed Lowest.
     */
    LOWER,

    /**
     * The lowest possible priority, used for Module functions that are not needed immediately.
     */
    LOWEST
}
