package com.thepanthurr.apolune.event;

/**
 * Event is a class used to mark Events, it also includes
 * basic cancellation functionality, used for denying
 * the method it the event is called it, from continuing
 * to run.
 *
 * @author thePanthurr
 * @version 1.0.0
 * @since 4/14/13
 */
public class Event {

    private boolean cancelled; //Holds the current cancellation value for the Event.

    /**
     * Used to set the cancelled state of the Event to
     * true or false. If set to true, the Event will deny
     * the method that it is called in from continuing
     * to run, and instead will return it immediately.
     *
     * @param cancelled The boolean value that the variable 'cancelled' is to be set to.
     */
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    /**
     * Used to see if the Event has been cancelled
     * or not. It is used in the methods where an event
     * is called in order to determine if the rest of
     * the method is allowed to run.
     *
     * @return cancelled
     *         The boolean holding the cancelled state of the Event.
     */
    public boolean isCancelled() {
        return cancelled;
    }
}
