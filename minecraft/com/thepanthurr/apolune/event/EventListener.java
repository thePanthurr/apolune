package com.thepanthurr.apolune.event;

/**
 * EventListener is a blank interface used for marking
 * which classes require events to be passed to them.
 *
 * @author thePanthurr
 * @version 1.0.0
 * @since 4/15/13
 */
public interface EventListener {

}
