package com.thepanthurr.apolune.event;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * EventHandler is an annotation used to mark methods
 * that are to be considered when firing an Event from
 * the EventManager.
 *
 * @author thePanthurr
 * @version 1.0.0
 * @since 4/14/13
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface EventHandler {
    public EventPriority priority() default EventPriority.DEFAULT;
}
