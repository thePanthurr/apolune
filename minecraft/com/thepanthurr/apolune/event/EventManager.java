package com.thepanthurr.apolune.event;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * EventManager is used to dispatch Event calls to Modules
 * and other extraneous classes.
 *
 * @author thePanthurr
 * @version 1.0.0
 * @since 4/14/13
 */
public class EventManager {

    private static volatile EventManager newInstance;                        //A variable for holding the single instance of EventManager.
    private Map<EventListener, List<Method>> listenerMap = new HashMap<EventListener, List<Method>>(); //A HashMap containing the list of annotated methods in an EventListener.

    /**
     * A Singleton method used to access the single instance of
     * EventManager, it is double-locked and thread-safe.
     *
     * @return newInstance
     *         A variable of type EventManager, only set to be other
     *         than null when this method is first called.
     */
    public static EventManager getInstance() {
        if (newInstance == null) {
            synchronized (EventManager.class) {
                if (newInstance == null) {
                    newInstance = new EventManager();
                }
            }
        }
        return newInstance;
    }

    /**
     * A method that adds a Class extending EventListener,
     * and an ArrayList of its methods that contain EventHandler
     * to the listenerMap HashMap.
     *
     * @param eventListener The class extending EventListener that contains the methods.
     */
    public void registerListener(EventListener eventListener) {
        List<Method> methodList = new ArrayList<Method>();

        for (Method method : eventListener.getClass().getDeclaredMethods()) {
            if (method.isAnnotationPresent(EventHandler.class)) {
                methodList.add(method);
            }
        }

        listenerMap.put(eventListener, methodList);
    }

    /**
     * A method that iterates through each of the registered
     * EventListeners, and runs the methods containing the
     * EventHandler annotation, in order according to their
     * priority.
     *
     * @param event The class extending Event that is being called.
     */
    public synchronized Event callEvent(final Event event) {
        for (EventPriority eventPriority : EventPriority.values()) {
            for (Entry<EventListener, Method> entry : getMethodMap(event).entrySet()) {
                if (eventPriority == entry.getValue().getAnnotation(EventHandler.class).priority()) {
                    try {
                        entry.getValue().invoke(entry.getKey(), event);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return event;
    }

    /**
     * An event that returns a list of the methods that should be run
     * and their EventListener counterparts, so that they can be invoked.
     *
     * @param event The class extending Event that is being called.
     * @return methodMap
     *         The HashMap containing individual entries for each EventListener -> Method pair.
     */
    public Map<EventListener, Method> getMethodMap(Event event) {
        Map<EventListener, Method> methodMap = new HashMap<EventListener, Method>();

        for (Entry<EventListener, List<Method>> entry : listenerMap.entrySet()) {
            for (Method method : entry.getValue()) {
                if (event.getClass().equals(method.getParameterTypes()[0])) {
                    methodMap.put(entry.getKey(), method);
                }
            }
        }

        return methodMap;
    }
}
