package com.thepanthurr.apolune.io;

/**
 * Vault is an interface used to define the basic structure
 * of a class intended to save and load values.
 *
 * @author thePanthurr
 * @version 1.0.1
 * @since 5/8/13
 */
public interface Vault {

    String breaker = "\t>>\t"; //Used to hold the separator that the client uses in its saving and loading.

    /**
     * A method used to take the values from the client,
     * and put them into a text file.
     */
    public void save();

    /**
     * A method used to take the values from the text file,
     * and put them into the client.
     */
    public void load();
}
