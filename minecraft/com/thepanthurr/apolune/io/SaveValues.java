package com.thepanthurr.apolune.io;

import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.module.Toggle;
import com.thepanthurr.apolune.module.Value;

import java.io.*;
import java.text.DecimalFormat;

/**
 * SaveValues is a Vault class used for handling the saving
 * and loading of Values.
 *
 * @author thePanthurr
 * @version 1.0.1
 * @since 5/8/13
 */
public class SaveValues implements Vault {

    private static volatile SaveValues newInstance;//A variable for holding the single instance of SaveValues.
    private File values;                           //A variable to hold the File containing the values.


    /**
     * A Singleton method used to access the single instance of
     * SaveValues, it is double-locked and thread-safe.
     *
     * @return newInstance
     *         A variable of type SaveValues, only set to be other
     *         than null when this method is first called.
     */
    public static SaveValues getInstance() {
        if (newInstance == null) {
            synchronized (SaveValues.class) {
                if (newInstance == null) {
                    newInstance = new SaveValues();
                }
            }
        }
        return newInstance;
    }

    @Override
    public void save() {
        DecimalFormat format = new DecimalFormat("#.##");
        try {
            if (values == null) {
                values = new File(Apolune.getInstance().getUtils().getApoluneDir() + "/Values.txt");
            }

            if (!values.exists()) {
                values.createNewFile();
            }

            PrintWriter writer = new PrintWriter(values);

            for (Toggle toggle : Apolune.getInstance().getToggleManager().getToggleList()) {
                if (!toggle.getValues().isEmpty()) {
                    for (Value value : toggle.getValues()) {
                        if (value.isSave()) {
                            writer.println(toggle.getName() + breaker + value.getKey() + breaker
                                    + (value.isBoolean() ? value.getStrings()[0] : format.format(value.getFloat()[0])));
                            System.out.println(toggle.getName() + breaker + value.getKey() + breaker
                                    + (value.isBoolean() ? value.getStrings()[0] : format.format(value.getFloat()[0])));
                        }
                    }
                }
            }

            writer.close();
        } catch (IOException ioexception) {
            ioexception.printStackTrace();
        }
    }

    @Override
    public void load() {
        try {
            if (values == null) {
                values = new File(Apolune.getInstance().getUtils().getApoluneDir() + "/Values.txt");
            }

            if (!values.exists()) {
                values.createNewFile();
            }

            BufferedReader bufferedreader = new BufferedReader(new FileReader(values));
            String string;
            while ((string = bufferedreader.readLine()) != null) {
                for (Toggle toggle : Apolune.getInstance().getToggleManager().getToggleList()) {
                    if (!toggle.getValues().isEmpty()) {
                        for (Value value : toggle.getValues()) {
                            if (string.startsWith(toggle.getName() + breaker + value.getKey())) {
                                toggle.getValue(string.split(breaker)[1]).setInitial(string.split(breaker)[2]);
                                System.out.println("Value successfully loaded" + breaker + value.getKey());
                            }
                        }
                    }
                }
            }

            bufferedreader.close();
        } catch (IOException ioexception) {
            ioexception.printStackTrace();
        }
    }

}
