package com.thepanthurr.apolune.io;

import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.module.Toggle;
import org.lwjgl.input.Keyboard;

import java.io.*;

/**
 * SaveKeys is a Vault class used for handling the saving
 * and loading of KeyBinds for modules.
 *
 * @author thePanthurr
 * @version 1.0.0
 * @since 5/8/13
 */
public class SaveKeys implements Vault {

    private static volatile SaveKeys newInstance;//A variable for holding the single instance of SaveKeys.
    private File keys;                           //A variable to hold the File containing the keys.


    /**
     * A Singleton method used to access the single instance of
     * SaveKeys, it is double-locked and thread-safe.
     *
     * @return newInstance
     *         A variable of type SaveKeys, only set to be other
     *         than null when this method is first called.
     */
    public static SaveKeys getInstance() {
        if (newInstance == null) {
            synchronized (SaveKeys.class) {
                if (newInstance == null) {
                    newInstance = new SaveKeys();
                }
            }
        }
        return newInstance;
    }

    @Override
    public void save() {
        try {
            if (keys == null) {
                keys = new File(Apolune.getInstance().getUtils().getApoluneDir() + "/Keys.txt");
            }

            if (!keys.exists()) {
                keys.createNewFile();
            }

            PrintWriter writer = new PrintWriter(keys);

            for (Toggle toggle : Apolune.getInstance().getToggleManager().getToggleList()) {
                writer.println(toggle.getName() + breaker + Keyboard.getKeyName(toggle.getKeyBind()));
                System.out.println(toggle.getName() + breaker + Keyboard.getKeyName(toggle.getKeyBind()));
            }

            writer.close();
        } catch (IOException ioexception) {
            ioexception.printStackTrace();
        }
    }

    @Override
    public void load() {
        try {
            if (keys == null) {
                keys = new File(Apolune.getInstance().getUtils().getApoluneDir() + "/Keys.txt");
            }

            if (!keys.exists()) {
                keys.createNewFile();
            }

            BufferedReader bufferedreader = new BufferedReader(new FileReader(keys));
            String string;
            while ((string = bufferedreader.readLine()) != null) {
                for (Toggle toggle : Apolune.getInstance().getToggleManager().getToggleList()) {
                    if (string.startsWith(toggle.getName())) {
                        toggle.setKeyBind(Keyboard.getKeyIndex(string.split(breaker)[1]));
                        System.out.println("KeyBind successfully loaded" + breaker + toggle.getName() + " is bound to"
                                + breaker + Keyboard.getKeyName(toggle.getKeyBind()));
                    }
                }
            }

            bufferedreader.close();
        } catch (IOException ioexception) {
            ioexception.printStackTrace();
        }
    }

}
