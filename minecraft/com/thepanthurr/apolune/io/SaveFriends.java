package com.thepanthurr.apolune.io;

import com.thepanthurr.apolune.Apolune;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * SaveFriends is a Vault class used for handling the
 * saving and loading of friends, as well as handling
 * the ArrayList that the friends are stored in.
 *
 * @author thePanthurr
 * @version 1.0.0
 * @since 5/13/13
 */
public class SaveFriends implements Vault {

    private static volatile SaveFriends newInstance;            //A variable for holding the single instance of SaveFriends.
    private File friends;                                       //A variable to hold the File containing the friends.
    private List<String> friendsList = new ArrayList<String>(); //Used to hold the usernames of your current friends.


    /**
     * A Singleton method used to access the single instance of
     * SaveFriends, it is double-locked and thread-safe.
     *
     * @return newInstance
     *         A variable of type SaveFriends, only set to be other
     *         than null when this method is first called.
     */
    public static SaveFriends getInstance() {
        if (newInstance == null) {
            synchronized (SaveFriends.class) {
                if (newInstance == null) {
                    newInstance = new SaveFriends();
                }
            }
        }
        return newInstance;
    }

    @Override
    public void save() {
        try {
            if (friends == null) {
                friends = new File(Apolune.getInstance().getUtils().getApoluneDir() + "/Friends.txt");
            }

            if (!friends.exists()) {
                friends.createNewFile();
            }

            PrintWriter writer = new PrintWriter(friends);

            for (String string : friendsList) {
                writer.println(string);
                System.out.println(string);
            }

            writer.close();
        } catch (IOException ioexception) {
            ioexception.printStackTrace();
        }
    }

    @Override
    public void load() {
        try {
            if (friends == null) {
                friends = new File(Apolune.getInstance().getUtils().getApoluneDir() + "/Friends.txt");
            }

            if (!friends.exists()) {
                friends.createNewFile();
            }

            BufferedReader bufferedreader = new BufferedReader(new FileReader(friends));
            String string;
            while ((string = bufferedreader.readLine()) != null) {
                friendsList.add(string);
                System.out.println("Friend successfully loaded" + breaker + string);
            }

            bufferedreader.close();
        } catch (IOException ioexception) {
            ioexception.printStackTrace();
        }
    }

    /**
     * Used by the command CommandFriend and in modules
     * to check if the username is a friend or not.
     *
     * @return friendsList
     *         The ArrayList in this class holding the
     *         currently registered usernames of your friends.
     */
    public List<String> getFriendsList() {
        return friendsList;
    }

    public boolean isFriend(String username) {
        boolean flag = false;
        for (String string : friendsList) {
            if (Apolune.getInstance().getUtils().stripColorCodes(username.toLowerCase())
                    .equalsIgnoreCase(Apolune.getInstance().getUtils().stripColorCodes(string.toLowerCase()))) {
                flag = true;
            }
        }

        return flag;
    }
}
