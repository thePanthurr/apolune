package com.thepanthurr.apolune;

import com.thepanthurr.apolune.TypeFont.FontType;
import com.thepanthurr.apolune.event.EventHandler;
import com.thepanthurr.apolune.event.EventListener;
import com.thepanthurr.apolune.event.EventManager;
import com.thepanthurr.apolune.event.events.EventGameStart;
import com.thepanthurr.apolune.event.events.EventUpdate;
import com.thepanthurr.apolune.io.SaveFriends;
import com.thepanthurr.apolune.io.SaveKeys;
import com.thepanthurr.apolune.io.SaveValues;
import com.thepanthurr.apolune.managers.CommandManager;
import com.thepanthurr.apolune.managers.GuiManager;
import com.thepanthurr.apolune.managers.ModuleManager;
import com.thepanthurr.apolune.managers.ToggleManager;
import net.minecraft.src.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Apolune is the main class for the Client modification,
 * it holds references to the other main classes, as well
 * as provides methods for accessing the Minecraft src.
 *
 * @author thePanthurr
 * @version 1.1.5
 * @since 4/14/13
 */
public class Apolune implements EventListener {

    private static volatile Apolune newInstance;                            //A variable for holding the single instance of Apolune.
    private Map<String, Object> settings = new HashMap<String, Object>() {{  //A variable for holding Client-Specific-Values.
        put("nocheat", false);
        put("firsttime", true);
    }};

    /**
     * A Singleton method used to access the single instance of
     * Apolune, it is double-locked and thread-safe.
     *
     * @return newInstance
     *         A variable of type Apolune, only set to be other
     *         than null when this method is first called.
     */
    public static Apolune getInstance() {
        if (newInstance == null) {
            synchronized (Apolune.class) {
                if (newInstance == null) {
                    newInstance = new Apolune();
                }
            }
        }
        return newInstance;
    }

    private Apolune() {
        getEventManager().registerListener(this);
        getEventManager().registerListener(getToggleManager());
        getEventManager().registerListener(getCommandManager());
        getEventManager().registerListener(getModuleManager());
    }

    /**
     * Used to fix some weird bug with GuiManager.
     *
     * @param eventGameStart An event parameter for EventHandler.
     */
    @EventHandler
    public void onStart(EventGameStart eventGameStart) {
        getEventManager().registerListener(getGuiManager());
        getSaveValues().load();
        getSaveKeys().load();
        getSaveFriends().load();
    }

    /**
     * Ran every tick, draws the 'Apolune' watermark
     * for the client in the top left of the screen.
     *
     * @param eventUpdate An event parameter for EventHandler.
     */
    @EventHandler
    public void onUpdate(EventUpdate eventUpdate) {
        Apolune.getInstance().getUtils().getDefaultFont().drawString("yeh", 2+10, 2+10, FontType.SHADOW_THICK, 0xFFFFFFFF, 0xFF000000);
    }

    /**
     * A Wrapper method used to access the single instance of
     * ToggleManager.
     *
     * @return ToggleManager.getInstance()
     *         The Singleton method held within ToggleManager.
     */
    public ToggleManager getToggleManager() {
        return ToggleManager.getInstance();
    }

    /**
     * A Wrapper method used to access the single instance of
     * CommandManager.
     *
     * @return CommandManager.getInstance()
     *         The Singleton method held within CommandManager.
     */
    public CommandManager getCommandManager() {
        return CommandManager.getInstance();
    }

    /**
     * A Wrapper method used to access the single instance of
     * GuiManager.
     *
     * @return GuiManager.getInstance()
     *         The Singleton method held within GuiManager.
     */
    public GuiManager getGuiManager() {
        return GuiManager.getInstance();
    }

    /**
     * A Wrapper method used to access the single instance of
     * EventManager.
     *
     * @return EventManager.getInstance()
     *         The Singleton method held within EventManager.
     */
    public EventManager getEventManager() {
        return EventManager.getInstance();
    }

    /**
     * A Wrapper method used to access the single instance of
     * ModuleManager.
     *
     * @return ModuleManager.getInstance()
     *         The Singleton method held within ModuleManager.
     */
    public ModuleManager getModuleManager() {
        return ModuleManager.getInstance();
    }

    /**
     * A Wrapper method used to access the single instance of
     * Utils.
     *
     * @return Utils.getInstance()
     *         The Singleton method held within Utils.
     */
    public Utils getUtils() {
        return Utils.getInstance();
    }

    /**
     * A method used to get the options from the Client so
     * that they may be utilized in saving or in a Module.
     *
     * @return settings
     *         The HashMap holding a key and and object,
     *         with the key being the name of the setting,
     *         and the object being the value of it.
     */
    public Map<String, Object> getSettings() {
        return settings;
    }

    /**
     * A method used to set an option in the Client.
     *
     * @param key   The string identifier by which the value of the
     *              setting is going to be referenced.
     * @param value The object that holds what the key references.
     */
    public void setSetting(String key, Object value) {
        if (settings.get(key) != null) {
            settings.put(key, value);
        }
    }

    /**
     * A Wrapper method used to access the single instance of
     * Minecraft.
     *
     * @return Minecraft.getMinecraft()
     *         The Singleton method held within Minecraft.
     */
    public Minecraft getMinecraft() {
        return Minecraft.getMinecraft();
    }

    /**
     * A get method used to access the 'thePlayer' variable
     * inside Minecraft.java.
     *
     * @return getMinecraft().thePlayer
     *         thePlayer is a variable holding the controller of the character in Minecraft.
     * @see #getMinecraft();
     */
    public EntityPlayer getPlayer() {
        return getMinecraft().thePlayer;
    }

    /**
     * A get method used to access the 'theWorld' variable
     * inside Minecraft.java.
     *
     * @return getMinecraft().theWorld
     *         theWorld is a variable holding the controller of the character in Minecraft.
     * @see #getMinecraft();
     */
    public WorldClient getWorld() {
        return getMinecraft().theWorld;
    }

    /**
     * A get method used to access the 'gameSettings' variable
     * inside Minecraft.java.
     *
     * @return getMinecraft().gameSettings
     *         gameSettings is a variable holding the instance of GameSettings in Minecraft.
     * @see #getMinecraft();
     */
    public GameSettings getGameSettings() {
        return getMinecraft().gameSettings;
    }

    /**
     * A get method used to access the 'playerController' variable
     * inside Minecraft.java.
     *
     * @return getMinecraft().playercontroller
     *         playerController is a variable holding the instance of PlayerControllerMP in Minecraft.
     * @see #getMinecraft();
     */
    public PlayerControllerMP getPlayerController() {
        return getMinecraft().playerController;
    }

    /**
     * A get method used to access the getNetHandler() method
     * inside Minecraft.java.
     *
     * @return getMinecraft().netHandler
     *         getNetHandler() is a method returning the instance of NetHandler in Minecraft.
     * @see #getMinecraft();
     */
    public NetClientHandler getNetHandler() {
        return getMinecraft().getNetHandler();
    }

    /**
     * A Wrapper method used to access the single instance of
     * SaveValues.
     *
     * @return SaveValues.getInstance()
     *         The Singleton method held within SaveValues.
     */
    public SaveValues getSaveValues() {
        return SaveValues.getInstance();
    }

    /**
     * A Wrapper method used to access the single instance of
     * SaveKeys.
     *
     * @return SaveKeys.getInstance()
     *         The Singleton method held within SaveKeys.
     */
    public SaveKeys getSaveKeys() {
        return SaveKeys.getInstance();
    }

    /**
     * A Wrapper method used to access the single instance of
     * SaveFriends.
     *
     * @return SaveFriends.getInstance()
     *         The Singleton method held within SaveFriends.
     */
    public SaveFriends getSaveFriends() {
        return SaveFriends.getInstance();
    }
}
