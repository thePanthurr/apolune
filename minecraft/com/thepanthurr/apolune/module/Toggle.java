package com.thepanthurr.apolune.module;

import com.thepanthurr.apolune.event.EventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Toggle is a base class that deals with the basic
 * handling of a toggleable module.
 *
 * @author thePanthurr/ThatNahr
 * @version 1.0.9
 * @since 4/14/13
 */
public abstract class Toggle implements Module, EventListener {

    private String name;                                    //Used to hold the name of the Toggle. Is displayed in the gui under this name.
    private String description;                             //Used to hold the description of the Toggle, mostly made for the user.
    private Category category;                              //Used to hold the category the Toggle is placed in. Controls color in the ArrayList and window in the GUI.
    private int keyBind;                                    //Used to hold the keybind that will toggle the Toggle on and off.
    private boolean toggled;                                //Used to hold the current state of the Toggle. Turned to true and to false with the toggle() method.
    private List<Value> values = new ArrayList<Value>();    //Used to hold all of the values of the Toggle that are changeable by the user.
    private int toggledTime;                                //Used for the fading ArrayList. Increases or decreases when toggled.

    public Toggle(String name, String description, Category category, int keyBind) {
        this.name = name;
        this.description = description;
        this.category = category;
        this.keyBind = keyBind;
        onRegister();
    }

    public Toggle(String name, int keyBind) {
        this(name, "A keybind.", Category.CLIENT, keyBind);
    }

    /**
     * An abstract method ran only when the Toggle is first added to the
     * ArrayList of the ToggleManager.
     */
    protected abstract void onRegister();

    /**
     * An abstract method ran only when the Toggle is turned on.
     */
    protected abstract void onEnable();

    /**
     * An abstract method ran only when the Toggle is turned off.
     */
    protected abstract void onDisable();

    /**
     * Used to turn on and off the Toggle, this method should be used
     * in place of a setToggled() or something of the like.
     */
    public void toggle() {
        toggled = !toggled;

        if (toggled) {
            onEnable();
        } else {
            onDisable();
        }
    }

    @Override
    public String getName() {
        return name;
    }

    /**
     * A get method for the description variable so that the
     * help command may display what the Toggle does.
     *
     * @return description
     *         The description String defined in the constructor.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Used to retrieve the category of the Toggle for GUI purposes as well as
     * to define the color in the ArrayList.
     *
     * @return category
     *         The Category of the Toggle defined in the constructor.
     */
    public Category getCategory() {
        return category;
    }

    /**
     * Used to retrieve the KeyBind of the Toggle, in order to handle
     * toggling.
     *
     * @return keyBind
     *         The integer keycode of the Keyboard letter of the Toggle
     *         defined in the constructor.
     */
    public int getKeyBind() {
        return keyBind;
    }

    /**
     * Sets the keybind of the hack so that it may be toggled by
     * the key of the user's choosing.
     *
     * @param keyBind The keycode of the Keyboard key that the user is
     *                binding to the hack.
     */
    public void setKeyBind(int keyBind) {
        this.keyBind = keyBind;
    }

    /**
     * Used to determine whether or not the Toggle is running, applied in UX
     * such as the GUI having a button that works correctly, or the ArrayList
     * correctly displaying the Toggle when it is enabled.
     *
     * @return toggled
     *         The state of the Toggle that will be changed when toggle() is called.
     */
    public boolean isToggled() {
        return toggled;
    }

    /**
     * A method used to add new values to the ArrayList.
     *
     * @param key       The string by which the Value is to be identified.
     * @param theValues The array of parameters for a Value in this order:
     *                  Initial, Maximum, Minimum, NoCheat;
     * @param save      The boolean determining whether or not the Value should be saved to a text file.
     */
    public void setValue(String key, String[] aliases, Object[] theValues, boolean save) {
        if (!values.contains(getValue(key))) {
            values.add(new Value(key, aliases, theValues, save));
        } else if (theValues[0] instanceof Boolean
                || !(Float.parseFloat(theValues[0].toString()) > Float.parseFloat(theValues[0].toString()))) {
            getValue(key).setInitial(theValues[0]);
        }
    }

    /**
     * A method used to retrieve the Value by name from the
     * ArrayList of values.
     *
     * @param key The name of the Value that will be searched for.
     * @return value if it found one by the name || null if there is no value registered.
     */
    public Value getValue(String key) {
        for (Value value : values) {
            if (value.getKey().equals(key)) {
                return value;
            }

            for (String string : value.getAliases()) {
                if (string.equalsIgnoreCase(key)) {
                    return value;
                }
            }
        }
        return null;
    }

    /**
     * A get method used to return the current
     * ArrayList of registered values so that other
     * classes may loop through them such as HelpCommand
     * and I/O classes.
     *
     * @return values
     *         The ArrayList variable defined in this class.
     */
    public List<Value> getValues() {
        return values;
    }

    /**
     * Method used to get the time this hack has
     * been on the ArrayList. Used for the fading.
     *
     * @return toggledTime
     *         - Time hack has been toggled.
     */
    public int getToggleTime() {
        return this.toggledTime;
    }

    /**
     * Method used to set the time this hack has
     * been on the ArrayList. Used for the fading.
     */
    public void setToggleTime(int delta) {
        this.toggledTime += delta;
    }
}
