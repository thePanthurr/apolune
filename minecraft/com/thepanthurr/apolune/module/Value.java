package com.thepanthurr.apolune.module;

/**
 * Value is a class used to hold a key, an object, a save setting,
 * and a max slider value. It is used to keep track of changeable
 * settings in Modules.
 *
 * @author thePanthurr
 * @version 1.0.4
 * @since 4/17/13
 */
public class Value {

    private String key;         //Used to retrieve the key from the array its held in, also decides the name it is changed by.
    private String[] aliases;   //Used to get the other names for the hack for use in game. Done with aliases so we keep things consistent internally.
    private Object[] values;    //The original value of the Module's value.
    private boolean save;       //Decides whether or not the value should be saved.

    public Value(String key, String[] aliases, Object[] values, boolean save) {
        this.key = key;
        this.aliases = aliases;
        this.values = values;
        this.save = save;
    }

    /**
     * Returns the name of the value so that it may
     * be used in a Command or accessed by a getOption method.
     *
     * @return key
     *         The name of the value defined in the constructor.
     */
    public String getKey() {
        return key;
    }

    /**
     * Returns the save boolean of the value so that it may
     * be used when trying to figure what values to save to
     * a text document.
     *
     * @return save
     *         The save boolean defined in the constructor.
     */
    public boolean isSave() {
        return save;
    }

    /**
     * Returns the integer values of both the Initial and
     * Max values of the option, defined in the constructor.
     *
     * @return new int[]
     *         Containing the 2 values parsed to an integer.
     */
   /** public int[] getInt() {
        int[] newArray = new int[values.length];
        for (int i = 0; i < values.length; i++) {
            newArray[i] = Integer.parseInt(values[i].toString());
        }
        return newArray;
    }
    
    */
    public int[] getInt() {
        int[] newArray = new int[values.length];
        for (int i = 0; i < values.length; i++) {
            newArray[i] = (int)Math.round(Float.parseFloat(values[i].toString()));
        }
        return newArray;
    }

    /**
     * Returns the float values of both the Initial and
     * Max values of the option, defined in the constructor.
     *
     * @return new float[]
     *         Containing the 2 values parsed to an float.
     */
    public float[] getFloat() {
        float[] newArray = new float[values.length];
        for (int i = 0; i < values.length; i++) {
            newArray[i] = Float.parseFloat(values[i].toString());
        }
        return newArray;
    }

    /**
     * Returns the double values of both the Initial and
     * Max values of the option, defined in the constructor.
     *
     * @return new double[]
     *         Containing the 2 values parsed to an double.
     */
    public double[] getDouble() {
        double[] newArray = new double[values.length];
        for (int i = 0; i < values.length; i++) {
            newArray[i] = Double.parseDouble(values[i].toString());
        }
        return newArray;
    }

    /**
     * Returns the boolean values of both the Initial and
     * Max values of the option, defined in the constructor.
     *
     * @return new boolean[]
     *         Containing the 2 values parsed to an boolean.
     */
    public boolean[] getBoolean() {
        boolean[] newArray = new boolean[values.length];
        for (int i = 0; i < values.length; i++) {
            newArray[i] = Boolean.parseBoolean(values[i].toString());
        }
        return newArray;
    }

    /**
     * Returns the long values of both the Initial and
     * Max values of the option, defined in the constructor.
     *
     * @return new long[]
     *         Containing the 2 values parsed to an long.
     */
    public long[] getLong() {
        long[] newArray = new long[values.length];
        for (int i = 0; i < values.length; i++) {
            newArray[i] = Long.parseLong(values[i].toString());
        }
        return newArray;
    }

    /**
     * Returns the string versions of both the Initial and
     * Max values of the option, defined in the constructor.
     *
     * @return new String[]
     *         Containing the 2 values converted to a String.
     */
    public String[] getStrings() {
        String[] newArray = new String[values.length];
        for (int i = 0; i < values.length; i++) {
            newArray[i] = values[i].toString();
        }
        return newArray;
    }

    /**
     * Used for changing an initial value during runtime in a Module.
     *
     * @param initial The number that is going to become the new initial value of the Value.
     */
    public void setInitial(Object initial) {
        this.values[0] = initial;
    }

    /**
     * Used for retrieving the other names for the value,
     * this is used in Commands when trying to modify the
     * value of a given Module.
     *
     * @return aliases
     *         The variable holding the String array of extra names.
     */
    public String[] getAliases() {
        return aliases;
    }

    /**
     * A simple method used to determine if the Value is a boolean or not
     * in the case of formatting or batch setting.
     *
     * @return getStrings()[0].equalsIgnoreCase("true") || getStrings()[0].equalsIgnoreCase("false");
     *         If the initial value is true or false.
     */
    public boolean isBoolean() {
        return getStrings()[0].equalsIgnoreCase("true") || getStrings()[0].equalsIgnoreCase("false");
    }
}
