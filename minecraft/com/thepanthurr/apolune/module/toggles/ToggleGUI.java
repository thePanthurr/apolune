package com.thepanthurr.apolune.module.toggles;

import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.module.Toggle;
import org.lwjgl.input.Keyboard;

/**
 * ToggleGUI is a keybind Toggle Module used for
 * toggling the GUI on.
 *
 * @author thePanthurr
 * @version 1.0.1
 * @since 4/28/13
 */
public class ToggleGUI extends Toggle {

    public ToggleGUI() {
        super("Gui", Keyboard.KEY_LEFT);
    }

    @Override
    protected void onRegister() {

    }

    @Override
    protected void onEnable() {
        toggle();
        Apolune.getInstance().getMinecraft().displayGuiScreen(Apolune.getInstance().getGuiManager());
    }

    @Override
    protected void onDisable() {

    }
}
