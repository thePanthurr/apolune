package com.thepanthurr.apolune.module.toggles;

import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.module.Category;
import com.thepanthurr.apolune.module.Toggle;
import org.lwjgl.input.Keyboard;

/**
 * ToggleNoCheat is a class used to control the NoCheat
 * setting in Apolune's settings.
 *
 * @author thePanthurr
 * @version 1.0.0
 * @since 4/23/13
 */
public class ToggleNoCheat extends Toggle {
    //TODO: Need to make this limit values of Sliders and the Set Command.

    public ToggleNoCheat() {
        super("NoCheat", "Makes the client compatible with NoCheat.", Category.CLIENT, Keyboard.KEY_NONE);
    }

    @Override
    protected void onRegister() {

    }

    @Override
    protected void onEnable() {
        Apolune.getInstance().setSetting("nocheat", true);
    }

    @Override
    protected void onDisable() {
        Apolune.getInstance().setSetting("nocheat", false);
    }
}
