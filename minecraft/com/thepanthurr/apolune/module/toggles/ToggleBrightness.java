package com.thepanthurr.apolune.module.toggles;

import org.lwjgl.input.Keyboard;
import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.event.EventHandler;
import com.thepanthurr.apolune.event.EventPriority;
import com.thepanthurr.apolune.event.events.EventUpdate;
import com.thepanthurr.apolune.module.Category;
import com.thepanthurr.apolune.module.Toggle;

/**
 * ToggleBrightness is a Module used to brighten the
 * surrounding world. It is used to allow the player
 * to see blocks he is mining underground or just make
 * it easier to see in general.
 *
 * @author thePanthurr/ThatNahr
 * @version 1.0.6
 * @since 4/22/13
 */
public class ToggleBrightness extends Toggle {

    private float curBright = 0; //Variable used for holding the gamma value when the user activates the hack. 		

    public ToggleBrightness() {
        super("Brightness", "Turns your gamma up so that you can see the world.", Category.WORLD, Keyboard.KEY_C);
    }

    @Override
    protected void onRegister() {
        setValue("light", null, new Object[]{2, 5, 1.1, 5}, true);
    }

    @Override
    protected void onEnable() {
        curBright = Apolune.getInstance().getGameSettings().gammaSetting;
    }

    @Override
    protected void onDisable() {

    }

    /**
     * This runs every tick, and checks if the
     * gammaValue needs to be updated back to
     * the "light" value.
     *
     * @param eventUpdate An event parameter for EventHandler.
     */
    @EventHandler(priority = EventPriority.LOWER)
    public void onUpdate(EventUpdate eventUpdate) {
        if (this.isToggled() && Apolune.getInstance().getGameSettings().gammaSetting
                < this.getValue("light").getFloat()[0]) {
            Apolune.getInstance().getGameSettings().gammaSetting += .2;
        } else if (!isToggled() && Apolune.getInstance().getGameSettings().gammaSetting
                > curBright) {
            Apolune.getInstance().getGameSettings().gammaSetting -= .25;
            if(Apolune.getInstance().getGameSettings().gammaSetting < 0) {
            	Apolune.getInstance().getGameSettings().gammaSetting = 0;
            }
        }
    }
}
