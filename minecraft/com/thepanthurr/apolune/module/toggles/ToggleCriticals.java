package com.thepanthurr.apolune.module.toggles;

import net.minecraft.src.EntityPlayerSP;

import org.lwjgl.input.Keyboard;

import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.event.EventHandler;
import com.thepanthurr.apolune.event.events.EventAttackEntity;
import com.thepanthurr.apolune.module.Category;
import com.thepanthurr.apolune.module.Toggle;

public class ToggleCriticals extends Toggle{

	public ToggleCriticals() {
		super("Criticals", "When attacking a player, you hit them with criticals.", Category.COMBAT, Keyboard.KEY_NONE);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onRegister() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void onEnable() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void onDisable() {
		// TODO Auto-generated method stub
		
	}
	
	@EventHandler
	public void onAttack(EventAttackEntity eventAttackEntity) {
		if(this.isToggled()) {// If the toggle is on
			if(Apolune.getInstance().getPlayer().onGround) {// If the player is on the ground when they attack
				eventAttackEntity.getEntityPlayer().motionY = 0.35;// Add to their Y.
				Apolune.getInstance().getPlayer().onCriticalHit(eventAttackEntity.getEntity());// Send a critical hit.
			}
		}
	}

}
