package com.thepanthurr.apolune.module.toggles;

import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.event.EventHandler;
import com.thepanthurr.apolune.event.EventPriority;
import com.thepanthurr.apolune.event.events.EventPostUpdate;
import com.thepanthurr.apolune.event.events.EventPreUpdate;
import com.thepanthurr.apolune.module.Category;
import com.thepanthurr.apolune.module.Toggle;
import net.minecraft.src.*;
import org.lwjgl.input.Keyboard;

import java.util.List;

/**
 * ToggleMobAura is a module that is used as a survival tool,
 * it kills surrounding creatures to make sure they aren't
 * going to kill the player..
 *
 * @author thePanthurr
 * @version 1.0.4
 * @since 5/19/13
 */
public class ToggleMobAura extends Toggle {
    private long start = 0L;        //A variable for holding the start time of the KillAura, used for delay.
    private long end = -1L;         //A variable for holding the end time of the KillAura, used for delay.
    private float curYaw, curPitch; //Variables used for holding the Player's current yaw and pitch, used for silent aimbot.

    public ToggleMobAura() {
        super("MobAura", "Kills monsters around you so that you don't get hurt.",
                Category.COMBAT, Keyboard.KEY_G);
    }

    @Override
    protected void onRegister() {
        setValue("attacks", new String[]{"aps", "a", "s", "sp"}, new Object[]{8, 20, 1, 15}, true);
        setValue("range", new String[]{"kr", "r"}, new Object[]{3.65, 5, 1, 4.4}, true);
        setValue("silent", new String[]{"sil", "aim"}, new Object[]{false, true, false, true}, true);
    }

    @Override
    protected void onEnable() {

    }

    @Override
    protected void onDisable() {

    }

    /**
     * This runs every tick and looks at the closest
     * mob, it then looks at them, and if enough time
     * has passed since the last attack, it will hit them
     * as well.
     *
     * @param eventPreUpdate An event parameter for EventHandler.
     */
    @EventHandler(priority = EventPriority.HIGHER)
    public void onPreUpdate(EventPreUpdate eventPreUpdate) {
        if (isToggled()) {
            start = System.nanoTime() / 1000000;
            if (getEntity() != null
                    && !Apolune.getInstance().getSaveFriends().isFriend(getEntity().getEntityName())) {
                curPitch = Apolune.getInstance().getPlayer().rotationPitch;
                curYaw = Apolune.getInstance().getPlayer().rotationYaw;
                if (Apolune.getInstance().getPlayer().getDistanceToEntity(getEntity())
                        <= getValue("range").getDouble()[0]) {

                    lookAtEntity(getEntity());
                    autoSword(getEntity());

                    if (isDelayed()) {
                        Apolune.getInstance().getPlayer().swingItem();
                        Apolune.getInstance().getPlayerController()
                                .attackEntity(Apolune.getInstance().getPlayer(), getEntity());
                        end = System.nanoTime() / 1000000;
                    }
                }
            }
        }
    }

    /**
     * This method runs every tick, makes the rotation
     * of the player appear normal, getting rid of
     * the aimbot, client-side.
     *
     * @param eventPostUpdate An event parameter for EventHandler.
     */
    @EventHandler
    public void onPostUpdate(EventPostUpdate eventPostUpdate) {
        if (this.isToggled()) {
            if (getValue("silent").getBoolean()[0]) {
                Apolune.getInstance().getPlayer().rotationPitch = curPitch;
                Apolune.getInstance().getPlayer().rotationYaw = curYaw;
                Apolune.getInstance().getPlayer().rotationYawHead = curYaw;
            }
        }
    }

    /**
     * This method returns the entity with the
     * highest threat for use in the running loop
     * to attack the correct entity.
     *
     * @return entityLiving
     *         The entity that has the highest threat.
     */
    public EntityLiving getEntity() {
        EntityLiving curEntity = null;
        for (Entity entity : (List<Entity>) Apolune.getInstance().getWorld().loadedEntityList) {
            if (entity instanceof EntityLiving && entity != Apolune.getInstance().getPlayer() && !(entity instanceof EntityPlayer)) {
                if (curEntity == null || Apolune.getInstance().getPlayer().getDistanceToEntity(entity)
                        < Apolune.getInstance().getPlayer().getDistanceToEntity(curEntity)) {
                    curEntity = (EntityLiving) entity;
                }
            }
        }
        return curEntity;
    }

    /**
     * Makes the player face the entity by
     * calculating the yaw and pitch needed to do so.
     *
     * @param entity The player which the user is supposed to
     *               look at.
     */
    private void lookAtEntity(Entity entity) {
        double x = entity.posX - Apolune.getInstance().getPlayer().posX;
        double y = (Apolune.getInstance().getPlayer().posY + Apolune.getInstance().getPlayer().getEyeHeight())
                - (entity.posY + entity.getEyeHeight());
        double z = entity.posZ - Apolune.getInstance().getPlayer().posZ;

        double yaw = Math.toDegrees(Math.atan2(z, x)) - 90;
        double pitch = Math.toDegrees(Math.atan2(y, Math.sqrt(x * x + z * z)));

        System.out.println("YAW : " + yaw);
        System.out.println("PLAYER YAW : " + Apolune.getInstance().getPlayer().rotationYaw);
        System.out.println("DISTANCE : " + getAngleDistance(Apolune.getInstance().getPlayer().rotationYaw, (float)yaw));
        if (getAngleDistance(Apolune.getInstance().getPlayer().rotationYaw, (float)yaw) > 20) {
            if (getAngleDistance(Apolune.getInstance().getPlayer().rotationYaw - 20, (float)yaw)
                    < getAngleDistance(Apolune.getInstance().getPlayer().rotationYaw + 20, (float)yaw)) {
                Apolune.getInstance().getPlayer().rotationYawHead -= 20;
                Apolune.getInstance().getPlayer().rotationYaw -= 20;
            } else {
                Apolune.getInstance().getPlayer().rotationYawHead += 20;
                Apolune.getInstance().getPlayer().rotationYaw += 20;

            }
        } else {
            Apolune.getInstance().getPlayer().rotationYaw = (float) yaw;
            Apolune.getInstance().getPlayer().rotationYawHead = (float) yaw;
        }
        Apolune.getInstance().getPlayer().rotationPitch = (float) pitch;
    }

    /**
     * This method checks the delay to make sure that it's okay to
     * swing at the Entity.
     *
     * @return start - end >=  1000 / getValue("attacks").getFloat()[0] + 2
     *         Whether or not enough time has passed.
     */
    public boolean isDelayed() {
        return start - end >= 1000 / getValue("attacks").getFloat()[0] + 2;
    }

    /**
     * This method gets the absolute distance between
     * 2 angles.
     *
     * @param angle  The first angle to be measured.
     * @param angle1 The second angle to find the distance between.
     * @return distance
     *         The distance between the two angles.
     */
    private float getAngleDistance(float angle, float angle1) {
        float distance = (Math.abs(angle - angle1)) % 360;

        if (distance > 180) {
            distance = (360 - distance);
        }

        return distance;
    }

    /**
     * This method switches the player's current item to one
     * that will do the most damage to the target entity. If
     * the player has no items that do more damage than the
     * current item, it won't switch.
     *
     * @param entity The Entity you plan to attack. Used to
     *               compare your current item's damage against
     *               the Entity.
     */
    private void autoSword(Entity entity) {
        float baseDamage = 1f;
        int curSlot = Apolune.getInstance().getPlayer().inventory.currentItem;
        for (int i = 0; i < 9; i++) {
            ItemStack itemStack = Apolune.getInstance().getPlayer().inventoryContainer.getSlot(i + 36).getStack();
            if (itemStack == null) {
                continue;
            }
            if (itemStack.getDamageVsEntity(entity) > baseDamage) {
                curSlot = i;
                baseDamage = itemStack.getDamageVsEntity(entity);
            }
            Apolune.getInstance().getPlayer().inventory.currentItem = curSlot;
        }
    }
}