package com.thepanthurr.apolune.module.toggles;

import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.event.EventHandler;
import com.thepanthurr.apolune.event.events.EventPreUpdate;
import com.thepanthurr.apolune.module.Category;
import com.thepanthurr.apolune.module.Toggle;
import org.lwjgl.input.Keyboard;

/**
 * ToggleSprint is a class used for player movement. It
 * will sprint when you press the forward button. This
 * makes it much easier to sprint, as the forward
 * button does not need to be double-tapped.
 *
 * @author ThatNahr/thePanthurr
 * @version 1.0.6
 * @since 4/22/13
 */
public class ToggleSprint extends Toggle {

    public ToggleSprint() {
        super("Sprint", "Forces your character to sprint when pressing your forward key.",
                Category.MOVEMENT, Keyboard.KEY_V);
    }

    @Override
    protected void onRegister() {
        this.setValue("modifier", new String[]{"m", "mod", "speed"}, new Object[]{1.25F, 10, 1.1F, 1.25F}, true);
    }

    @Override
    protected void onEnable() {

    }

    @Override
    protected void onDisable() {

    }

    /**
     * This runs before the motion updates are sent,
     * and modifies the player's speed. This also makes
     * the player start sprinting when the forward key
     * is pressed.
     *
     * @param eventPreUpdate An event parameter for EventHandler.
     */
    @EventHandler
    public void onPreUpdate(EventPreUpdate eventPreUpdate) {
        if (this.isToggled()) {
            if (Apolune.getInstance().getGameSettings().keyBindForward.pressed && !Apolune.getInstance().getPlayer().isCollidedHorizontally) {
                Apolune.getInstance().getPlayer().setSprinting(true);
            }

            if (Apolune.getInstance().getPlayer().isSprinting()) {
                Apolune.getInstance().getPlayer().jumpMovementFactor *= this.getValue("modifier").getFloat()[0];
                Apolune.getInstance().getPlayer().landMovementFactor *= this.getValue("modifier").getFloat()[0];
            }
        }
    }

}
