package com.thepanthurr.apolune.module.toggles;

import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.event.EventHandler;
import com.thepanthurr.apolune.event.events.EventPostUpdate;
import com.thepanthurr.apolune.event.events.EventPreUpdate;
import com.thepanthurr.apolune.module.Category;
import com.thepanthurr.apolune.module.Toggle;
import org.lwjgl.input.Keyboard;

/**
 * ToggleKaiStep is a class used for player movement. It
 * will step up blocks for you, without the need to
 * jump.
 *
 * @author ThatNahr/thePanthurr
 * @version 1.0.7
 * @since 4/22/13
 */
public class ToggleKaiStep extends Toggle {

    public ToggleKaiStep() {
        super("KaiStep", "Allows you to set your step height so you may walk up blocks like stairs.",
                Category.MOVEMENT, Keyboard.KEY_B);
    }

    @Override
    protected void onRegister() {
        this.setValue("height", new String[]{"sh", "h"}, new Object[]{1.5F, 8, 1F, 1F}, true);
    }

    @Override
    protected void onEnable() {
        Apolune.getInstance().getPlayer().stepHeight = this.getValue("height").getFloat()[0];
    }

    @Override
    protected void onDisable() {
        Apolune.getInstance().getPlayer().stepHeight = 0.5f;
    }

    /**
     * This runs before the motion updates are sent. It
     * checks to see if the client is in NoCheatPlus
     * compatibility mode. If so, it will make the
     * server think the player is taking more time to
     * step up the block.
     *
     * @param eventPreUpdate An event parameter for EventHandler.
     */
    @EventHandler
    public void onPreUpdate(EventPreUpdate eventPreUpdate) {
        if (this.isToggled()) {
            if ((Boolean) Apolune.getInstance().getSettings().get("nocheat")) {
                if (Apolune.getInstance().getPlayer().isCollidedHorizontally
                        && Apolune.getInstance().getPlayer().onGround) {
                    Apolune.getInstance().getPlayer().boundingBox.minY += 1.25;
                    Apolune.getInstance().getPlayer().posY += 1.25;
                }
            } else {
                Apolune.getInstance().getPlayer().stepHeight = this.getValue("height").getFloat()[0];
            }
        }
    }

    /**
     * This runs after the motion updates are sent. It
     * and makes the client appear to step up the
     * blocks normally, as opposed to "flying" up them.
     *
     * @param eventPostUpdate An event parameter for EventHandler.
     */
    @EventHandler
    public void onPostUpdate(EventPostUpdate eventPostUpdate) {
        if (this.isToggled()) {
            if ((Boolean) Apolune.getInstance().getSettings().get("nocheat")) {
                if (Apolune.getInstance().getPlayer().isCollidedHorizontally) {
                    Apolune.getInstance().getPlayer().boundingBox.minY -= 1.25;
                    Apolune.getInstance().getPlayer().posY -= 1.25;
                }
            } else {
                Apolune.getInstance().getPlayer().stepHeight = this.getValue("height").getFloat()[0];
            }
        } else {
            Apolune.getInstance().getPlayer().stepHeight = 0.5F;
        }
    }

}
