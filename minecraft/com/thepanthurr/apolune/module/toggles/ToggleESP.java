package com.thepanthurr.apolune.module.toggles;

import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.event.EventHandler;
import com.thepanthurr.apolune.event.events.EventRender;
import com.thepanthurr.apolune.module.Category;
import com.thepanthurr.apolune.module.Toggle;

import net.minecraft.src.AxisAlignedBB;
import net.minecraft.src.Entity;
import net.minecraft.src.EntityLiving;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.RenderManager;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import java.util.List;

/**
 * ToggleESP is a Module used to track players around you
 * without need for a radar. It's supposed to simulate a
 * 360 degree vision, becuase you know where everyone is
 * relative to you.
 *
 * @author thePanthurr/ThatNahr
 * @version 1.0.4
 * @since 6/5/13
 */
public class ToggleESP extends Toggle {

    public ToggleESP() {
        super("ESP", "Draws boxes or lines to players nearby.", Category.WORLD, Keyboard.KEY_M);
    }

    @Override
    protected void onRegister() {
        this.setValue("lines", new String[]{"l", "t", "tracers"}, new Object[]{true, true, false, true}, true);
        this.setValue("boxes", new String[]{"b"}, new Object[]{true, true, false, true}, true);
        this.setValue("range", new String[]{"mr", "mrange", "mobr"}, new Object[]{20, 64, 1, 64}, true);
    }

    @Override
    protected void onEnable() {

    }

    @Override
    protected void onDisable() {

    }

    /**
     * The method that handles the drawing of ESPs,
     * lines or boxes, depending on what is enabled
     * when it is time to draw it.
     *
     * @param eventRender An event parameter for EventHandler.
     */
    @EventHandler
    public void onRender(EventRender eventRender) {
        if (this.isToggled()) {
            if (this.getValue("lines").getBoolean()[0]) {
                this.lines();
            }
            if (this.getValue("boxes").getBoolean()[0]) {
                this.boxes();
            }
        }
    }

    /**
     * This method handles the drawing of OPENGL lines to the entities
     * in the world that are loaded each tick. They also fade based on
     * distance.
     */
    private void lines() {
        for (Entity entity : (List<Entity>) Apolune.getInstance().getWorld().loadedEntityList) {
            if (entity.equals(Apolune.getInstance().getPlayer())) {
                continue;
            }
            double xDiff = entity.posX - RenderManager.renderPosX;
            double yDiff = entity.posY - RenderManager.renderPosY + 1.5;
            double zDiff = entity.posZ - RenderManager.renderPosZ;
            double distance = Apolune.getInstance().getPlayer().getDistanceToEntity(entity);
            boolean friend = false;
            if (entity instanceof EntityPlayer) {
                EntityPlayer entityPlayer = (EntityPlayer) entity;
                friend = Apolune.getInstance().getSaveFriends().isFriend(entityPlayer.username);
            }
            if (entity.isDead || !(entity instanceof EntityLiving)
                    || (!(entity instanceof EntityPlayer) && distance > this.getValue("range").getFloat()[0])) {
                continue;
            }
            Apolune.getInstance().getUtils().setupForRender();
            GL11.glEnable(GL11.GL_LINE_SMOOTH);
            GL11.glHint(GL11.GL_LINE_SMOOTH_HINT, GL11.GL_DONT_CARE);
            float red = 1f;
            float green = 0f;
            float blue = 0.1f;
            if (friend) {
                // 0xFF99CCFF
                // Red: 153, Green: 204, Blue: 255, Alpha: 255
                GL11.glColor4f(153f / 255f, 204f / 255f, 1f, 1f);
            } else {
                float greenDelta = (float) ((distance - 3.5) / 32);
                float redDeltaSubtract = (float) ((distance - 35.5) / 32);
                if (greenDelta >= 0) {
                    green += greenDelta;
                }
                if (green > 1f) {
                    green = 1f;
                    if (redDeltaSubtract >= 0) {
                        red -= redDeltaSubtract;
                    }
                }
                if (red < 0) {
                    red = 0;
                }
                GL11.glColor4f(red, green, blue, 1f);
            }
            GL11.glLineWidth(1.5f);
            GL11.glBegin(GL11.GL_LINES);
            GL11.glVertex2d(0, 0);
            GL11.glVertex3d(xDiff, yDiff, zDiff);
            GL11.glEnd();
            Apolune.getInstance().getUtils().finishRender();
            GL11.glDisable(GL11.GL_LINE_SMOOTH);
        }
    }

    /**
     * This method handles the drawing of OPENGL boxes to the entities
     * in the world that are loaded each tick.
     */
    private void boxes() {
        for (Entity entity : (List<Entity>) Apolune.getInstance().getWorld().loadedEntityList) {
            if (entity.equals(Apolune.getInstance().getPlayer())) {
                continue;
            }
            double xDiff = entity.posX - RenderManager.renderPosX;
            double yDiff = entity.posY - RenderManager.renderPosY;
            double zDiff = entity.posZ - RenderManager.renderPosZ;
            double entityWidth = entity.width / 1.5;
            double entityHeight = entity.height + 0.2;
            double distance = Apolune.getInstance().getPlayer().getDistanceToEntity(entity);
            boolean friend = false;
            if (entity instanceof EntityPlayer) {
                EntityPlayer entityPlayer = (EntityPlayer) entity;
                friend = Apolune.getInstance().getSaveFriends().isFriend(entityPlayer.username);
            }
            if (entity.isDead || !(entity instanceof EntityLiving)
                    || !(entity instanceof EntityPlayer) && distance > this.getValue("mobrange").getFloat()[0]) {
                continue;
            }
            Apolune.getInstance().getUtils().setupForRender();
            GL11.glEnable(GL11.GL_LINE_SMOOTH);
            GL11.glHint(GL11.GL_LINE_SMOOTH_HINT, GL11.GL_DONT_CARE);
            if (friend) {
                // 0xFF99CCFF
                // Red: 153, Green: 204, Blue: 255, Alpha: 255
                GL11.glColor4f(153f / 255f, 204f / 255f, 1f, 1f);
            } else if (entity.hurtResistantTime >= 15) {
                // 0xFFE40000
                // Red: 228, Green: 0, Blue: 0, Alpha: 255
                GL11.glColor4f(228f / 255f, 0f, 0f, 1f);
            } else if (entity.hurtResistantTime > 0) {
                // 0xFFFF5200
                // Red: 255, Green: 82, Blue: 0, Alpha: 255
                GL11.glColor4f(1f, 82f / 255f, 0f, 1f);
            } else {
                // 0xFF10E114
                // Red: 16, Green: 225, Blue: 20, Alpha: 255
                GL11.glColor4f(16f / 255f, 225f / 255f, 20f / 255f, 1f);
            }
            GL11.glLineWidth(1.5f);
            AxisAlignedBB axisAlignedBB = new AxisAlignedBB(xDiff - entityWidth, yDiff - 0.1, zDiff - entityWidth,
                    xDiff + entityWidth, yDiff + entityHeight, zDiff + entityWidth);
            Apolune.getInstance().getUtils().drawOutlinedBoundingBox(axisAlignedBB);
            Apolune.getInstance().getUtils().finishRender();
            GL11.glDisable(GL11.GL_LINE_SMOOTH);
        }
    }

}
