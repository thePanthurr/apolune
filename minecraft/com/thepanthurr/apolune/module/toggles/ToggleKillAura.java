package com.thepanthurr.apolune.module.toggles;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.minecraft.src.EntityLivingBase;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.ItemStack;

import org.lwjgl.input.Keyboard;

import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.event.EventHandler;
import com.thepanthurr.apolune.event.EventPriority;
import com.thepanthurr.apolune.event.events.EventPostUpdate;
import com.thepanthurr.apolune.event.events.EventPreUpdate;
import com.thepanthurr.apolune.module.Category;
import com.thepanthurr.apolune.module.Toggle;

/**
 * ToggleKillAura is a class used for player combat, it swings
 * your sword around you, and hits the player with the highest
 * threat.
 *
 * @author thePanthurr/ThatNahr
 * @version 1.1.2
 * @since 4/17/13
 */
public class ToggleKillAura extends Toggle {

    private long start = 0L;                                                            //A variable for holding the start time of the KillAura, used for delay.
    private long end = -1L;                                                             //A variable for holding the end time of the KillAura, used for delay.
    private float curYaw, curPitch;                                                     //Variables used for holding the Player's current yaw and pitch, used for silent aimbot.
    private Map<EntityPlayer, Float> playerThreat = new HashMap<EntityPlayer, Float>(); //A variable used to hold each players threat.

    public ToggleKillAura() {
        super("KillAura", "Hits players around you at a very fast rate so you have a PvP advantage.",
                Category.COMBAT, Keyboard.KEY_F);
    }

    @Override
    protected void onRegister() {
        setValue("attacks", new String[]{"aps", "a", "s", "sp"}, new Object[]{8, 20, 1, 15}, true);
        setValue("range", new String[]{"kr", "r"}, new Object[]{3.65, 5, 1, 4.4}, true);
        setValue("silent", new String[]{"sil", "aim"}, new Object[]{false, true, false, true}, true);
    }

    @Override
    protected void onEnable() {

    }

    @Override
    protected void onDisable() {

    }

    /**
     * This runs every movement update and looks at the player
     * with the highest threat, it then looks at them,
     * and if enough time has passed, it will hit them
     * as well.
     *
     * @param eventPreUpdate An event parameter for EventHandler.
     */
    @EventHandler(priority = EventPriority.HIGHER)
    public void onPreUpdate(EventPreUpdate eventPreUpdate) {
        if (isToggled()) {
            start = System.nanoTime() / 1000000;
            assignThreat();
            if (getEntity() != null
                    && !Apolune.getInstance().getSaveFriends().isFriend(getEntity().username)) {
                curPitch = Apolune.getInstance().getPlayer().rotationPitch;
                curYaw = Apolune.getInstance().getPlayer().rotationYaw;
                if (Apolune.getInstance().getPlayer().getDistanceToEntity(getEntity())
                        <= getValue("range").getDouble()[0]) {
                    //autoSword(getEntity());
                    if (lookAtEntity(getEntity())) {
                        if (isDelayed()) {
                            Apolune.getInstance().getPlayer().swingItem();
                            Apolune.getInstance().getPlayerController()
                                    .attackEntity(Apolune.getInstance().getPlayer(), getEntity());
                            end = System.nanoTime() / 1000000;
                        }
                    }
                }
            }
        }
    }

    /**
     * This method runs every tick, makes the rotation
     * of the player appear normal, getting rid of
     * the aimbot, client-side.
     *
     * @param eventPostUpdate An event parameter for EventHandler.
     */
    @EventHandler
    public void onPostUpdate(EventPostUpdate eventPostUpdate) {
        if (this.isToggled()) {
            if (getValue("silent").getBoolean()[0]) {
                Apolune.getInstance().getPlayer().rotationPitch = curPitch;
                Apolune.getInstance().getPlayer().rotationYaw = curYaw;
                Apolune.getInstance().getPlayer().rotationYawHead = curYaw;
            }
        }
    }

    /**
     * This method loads each player into a HashMap with a float
     * value for their threat. It is used in determining which
     * entity to hit.
     */
    private void assignThreat() {
        playerThreat.clear();
        for (EntityPlayer entityPlayer : (List<EntityPlayer>) Apolune.getInstance().getWorld().playerEntities) {
            if (entityPlayer != Apolune.getInstance().getPlayer()) {
                float degreeThreat = 0;
                float distanceThreat = 0;
                float criticalThreat = 0;
                float angle = getAngleDistance(Apolune.getInstance().getPlayer().rotationYaw, (getPerfectPlayerYaw(entityPlayer)));
                if (angle <= 90) {
                    if (angle <= 5) {
                        degreeThreat = 3.5F;
                    } else {
                        degreeThreat = 17 / angle;
                    }
                }

                if (degreeThreat > 3.5F) {
                    degreeThreat = 3.5F;
                }
                if (degreeThreat < 0) {
                    degreeThreat = 0;
                }

                if (!entityPlayer.onGround) {
                    criticalThreat = 1;
                }

                float armorThreat = entityPlayer.getTotalArmorValue() / 6.86F;
                float weaponThreat = 2 / getEntityDamage(entityPlayer);

                if (Apolune.getInstance().getPlayer().getDistanceToEntity(entityPlayer) <= getValue("range").getFloat()[0]) {
                    distanceThreat = getValue("range").getFloat()[0] / Apolune.getInstance().getPlayer().getDistanceToEntity(entityPlayer);
                }
                float totalThreat = weaponThreat + degreeThreat + distanceThreat + criticalThreat - armorThreat;
                playerThreat.put(entityPlayer, totalThreat >= 0 ? totalThreat : 0);
            }
        }
    }

    /**
     * This method returns the entity with the
     * highest threat for use in the running loop
     * to attack the correct entity.
     *
     * @return entityPlayer
     *         The entity that has the highest threat.
     */
    public EntityPlayer getEntity() {
        EntityPlayer entityPlayer = null;
        for (Entry<EntityPlayer, Float> entry : playerThreat.entrySet()) {
            if (entry.getKey() != null && entry.getKey() != Apolune.getInstance().getPlayer()) {
                if (entityPlayer == null || entry.getValue() > playerThreat.get(entityPlayer)) {
                    entityPlayer = entry.getKey();
                }
            }
        }
        return entityPlayer;
    }

    /**
     * Makes the player face the entityPlayer by
     * calculating the yaw and pitch needed to do so.
     *
     * @param entityPlayer The player which the user is supposed to
     *                     look at.
     */
    private boolean lookAtEntity(EntityPlayer entityPlayer) {
        double x = entityPlayer.posX - Apolune.getInstance().getPlayer().posX;
        double y = (Apolune.getInstance().getPlayer().posY + Apolune.getInstance().getPlayer().getEyeHeight())
                - (entityPlayer.posY + entityPlayer.getEyeHeight());
        double z = entityPlayer.posZ - Apolune.getInstance().getPlayer().posZ;

        double yaw = Math.toDegrees(Math.atan2(z, x)) - 90;
        double pitch = Math.toDegrees(Math.atan2(y, Math.sqrt(x * x + z * z)));

        if (getAngleDistance(Apolune.getInstance().getPlayer().rotationYaw, (float)yaw) > 20) {
            if (getAngleDistance(Apolune.getInstance().getPlayer().rotationYaw - 20, (float)yaw)
                    < getAngleDistance(Apolune.getInstance().getPlayer().rotationYaw + 20, (float)yaw)) {
                Apolune.getInstance().getPlayer().rotationYawHead -= 20;
                Apolune.getInstance().getPlayer().rotationYaw -= 20;
            } else {
                Apolune.getInstance().getPlayer().rotationYawHead += 20;
                Apolune.getInstance().getPlayer().rotationYaw += 20;

            }
        } else {
            Apolune.getInstance().getPlayer().rotationYaw = (float) yaw;
            Apolune.getInstance().getPlayer().rotationYawHead = (float) yaw;
        }
        Apolune.getInstance().getPlayer().rotationPitch = (float) pitch;

        return Apolune.getInstance().getPlayer().rotationYaw == (float) yaw
                && Apolune.getInstance().getPlayer().rotationPitch == (float) pitch;
    }

    /**
     * This method checks the delay to make sure that it's okay to
     * swing at the Entity.
     *
     * @return start - end >=  1000 / getValue("attacks").getFloat()[0] + 2
     *         Whether or not enough time has passed.
     */
    public boolean isDelayed() {
        return start - end >= 1000 / getValue("attacks").getFloat()[0] + 2;
    }

    /**
     * Gets the player's damage to the entity.
     *
     * @param entityPlayer The player who's damage is trying to be calculated.
     * @return entityPlayer#getDamageVsEntity if he has an item
     *         || 1 if there is no item in his hand
     */
    private int getEntityDamage(EntityPlayer entityPlayer) {
        if (entityPlayer.getCurrentEquippedItem() != null) {
            return entityPlayer.getCurrentEquippedItem().getDamageVsEntity(Apolune.getInstance().getPlayer());
        }
        return 1;
    }

    /**
     * This method gets the absolute distance between
     * 2 angles.
     *
     * @param angle  The first angle to be measured.
     * @param angle1 The second angle to find the distance between.
     * @return distance
     *         The distance between the two angles.
     */
    private float getAngleDistance(float angle, float angle1) {
        float distance = (Math.abs(angle - angle1)) % 360;

        if (distance > 180) {
            distance = (360 - distance);
        }

        return distance;
    }

    /**
     * This method returns the yaw distance from you to
     * the specified entity.
     *
     * @param entityLiving The entity of which you will be comparing your yaw
     *                     to the position of.
     * @return yaw
     *         The yaw that is calculated between the player and
     *         the entityLiving.
     */
    private float getPerfectPlayerYaw(EntityLivingBase entityLiving) {
        double x = entityLiving.posX - Apolune.getInstance().getPlayer().posX;
        double z = entityLiving.posZ - Apolune.getInstance().getPlayer().posZ;
        return (float) Math.toDegrees(Math.atan2(z, x)) - 90;
    }

    /**
     * This method switches the player's current item to one
     * that will do the most damage to the target entity. If
     * the player has no items that do more damage than the
     * current item, it won't switch.
     *
     * @param entityPlayer The EntityPlayer you plan to attack. Used to
     *                     compare your current item's damage against
     *                     the EntityPlayer.
     */
    private void autoSword(EntityPlayer entityPlayer) {
        float baseDamage = 1f;
        int curSlot = Apolune.getInstance().getPlayer().inventory.currentItem;
        for (int i = 0; i < 9; i++) {
            ItemStack itemStack = Apolune.getInstance().getPlayer().inventoryContainer.getSlot(i + 36).getStack();
            if (itemStack == null) {
                continue;
            }
            if (itemStack.getDamageVsEntity(entityPlayer) > baseDamage) {
                curSlot = i;
                baseDamage = itemStack.getDamageVsEntity(entityPlayer);
            }
            Apolune.getInstance().getPlayer().inventory.currentItem = curSlot;
        }
    }

}