package com.thepanthurr.apolune.module.toggles;

import org.lwjgl.input.Keyboard;

import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.event.EventHandler;
import com.thepanthurr.apolune.event.events.EventPostUpdate;
import com.thepanthurr.apolune.event.events.EventPreUpdate;
import com.thepanthurr.apolune.module.Category;
import com.thepanthurr.apolune.module.Toggle;

/**
 * ToggleNoFall is a class used for player movement. It
 * prevents the player from taking damage from falling
 * from too high of a distance.
 *
 * @author ThatNahr
 * @version 1.0.1
 * @since 4/23/2013
 */
public class ToggleNoFall extends Toggle {

    private boolean curOnGround; //Variable used to holding the player's current onGround status to prevent the infinite jump glitch.

    public ToggleNoFall() {
        super("NoFall", "Prevents fall damage.", Category.MOVEMENT, Keyboard.KEY_N);
    }

    @Override
    protected void onRegister() {
    }

    @Override
    protected void onEnable() {
    }

    @Override
    protected void onDisable() {
    }

    /**
     * This runs before the motion updates are sent,
     * and makes the server think the player is
     * always on the ground.
     *
     * @param eventPreUpdate An event parameter for EventHandler.
     */
    @EventHandler
    public void onPreUpdate(EventPreUpdate eventPreUpdate) {
        if (this.isToggled()) {
            this.curOnGround = Apolune.getInstance().getPlayer().onGround;
            Apolune.getInstance().getPlayer().onGround = true;
        }
    }

    /**
     * This runs after the motion updates are sent,
     * and makes the client act as normal. This
     * is used to prevent infinite jumps.
     *
     * @param eventPostUpdate An event parameter for EventHandler.
     */
    @EventHandler
    public void onPostUpdate(EventPostUpdate eventPostUpdate) {
        if (this.isToggled()) {
            Apolune.getInstance().getPlayer().onGround = this.curOnGround;
        }
    }

}
