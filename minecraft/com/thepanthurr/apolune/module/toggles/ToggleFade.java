package com.thepanthurr.apolune.module.toggles;

import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.event.EventHandler;
import com.thepanthurr.apolune.event.events.EventUpdate;
import com.thepanthurr.apolune.module.Category;
import com.thepanthurr.apolune.module.Toggle;
import org.lwjgl.input.Keyboard;

/**
 * ToggleFade is a modified sneak hack that only turns on
 * when the player is not moving, good for afk-ing, easier
 * to have on all the time.
 *
 * @author thePanthurr/ThatNahr
 * @version 1.0.2
 * @since 5/1/13
 */
public class ToggleFade extends Toggle {

    public ToggleFade() {
        super("Fade", "Automatically makes you sneak when you are not moving.", Category.MOVEMENT, Keyboard.KEY_Z);
    }

    @Override
    protected void onRegister() {

    }

    @Override
    protected void onEnable() {

    }

    @Override
    protected void onDisable() {
        if (!Keyboard.isKeyDown(Apolune.getInstance().getGameSettings().keyBindSneak.keyCode)) {
            Apolune.getInstance().getGameSettings().keyBindSneak.pressed = false;
        }
    }

    /**
     * This method runs every tick and sets sneaking to true if they are
     * standing still.
     *
     * @param eventUpdate An event parameter for EventHandler.
     */
    @EventHandler
    public void onUpdate(EventUpdate eventUpdate) {
        if (isToggled()) {
            if ((Apolune.getInstance().getPlayer().motionX == 0
                    && Apolune.getInstance().getPlayer().motionZ == 0
                    && Apolune.getInstance().getPlayer().onGround)
                    || Keyboard.isKeyDown(Apolune.getInstance().getGameSettings().keyBindSneak.keyCode)) {
                Apolune.getInstance().getGameSettings().keyBindSneak.pressed = true;
            } else if (!Keyboard.isKeyDown(Apolune.getInstance().getGameSettings().keyBindSneak.keyCode)) {
                Apolune.getInstance().getGameSettings().keyBindSneak.pressed = false;
            }
        }
    }
}
