package com.thepanthurr.apolune.module.toggles;

import org.lwjgl.input.Keyboard;

import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.event.EventHandler;
import com.thepanthurr.apolune.event.events.EventVelocity;
import com.thepanthurr.apolune.module.Category;
import com.thepanthurr.apolune.module.Toggle;


/**
 * ToggleAntiVelocity is a class used for player movement.
 * It stops any effects that would modify the velocity
 * of the player. Useful for combat, general griefing,
 * or even just playing around.
 *
 * @author ThatNahr
 * @version 1.0.1
 * @since 5/7/13
 */
public class ToggleAntiVelocity extends Toggle {

    public ToggleAntiVelocity() {
        super("AntiVelocity", "Stops velocity effects targeted at the player.",
                Category.MOVEMENT, Keyboard.KEY_NONE);
    }

    @Override
    protected void onRegister() {

    }

    @Override
    protected void onEnable() {

    }

    @Override
    protected void onDisable() {

    }

    /**
     * This runs when an entity is in flowing liquids, or
     * when an EntityVelocity packet is received that
     * targets an entity. It checks if the entity is the
     * player, and if so, will cancel the effects.
     *
     * @param eventVelocity An event parameter for EventHandler.
     */
    @EventHandler
    public void onVelocityAffect(EventVelocity eventVelocity) {
        if (this.isToggled() && eventVelocity.getAffectedEntity().equals(Apolune.getInstance().getPlayer())) {
            eventVelocity.setCancelled(true);
        }
    }

}
