package com.thepanthurr.apolune.module.toggles;


import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.event.EventHandler;
import com.thepanthurr.apolune.event.events.EventRender;
import com.thepanthurr.apolune.event.events.EventUpdate;
import com.thepanthurr.apolune.module.Category;
import com.thepanthurr.apolune.module.Toggle;
import net.minecraft.src.RenderManager;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import java.util.ArrayList;

/**
 * ToggleBreadCrumbs is a class used for rendering
 * a trail, or "bread crumbs," wherever the player
 * goes.
 *
 * @author ThatNahr
 * @version 1.0.5
 * @since 4/24/13
 */
public class ToggleBreadCrumbs extends Toggle {

    private ArrayList<Double[]> positions = new ArrayList<Double[]>(); //ArrayList used to hold the positions for drawing the trail.
    private double lastX, lastY, lastZ;                               //Variables used to hold the positions that the player has most recently been through. Used to not overload the ArrayList.

    public ToggleBreadCrumbs() {
        super("BreadCrumbs", "Leaves a trail wherever the player goes.", Category.WORLD, Keyboard.KEY_NONE);
    }

    @Override
    protected void onRegister() {
        this.setValue("clear", new String[]{"cl", "c"}, new Object[]{false, true, false, false}, false);
    }

    @Override
    protected void onEnable() {

    }

    @Override
    protected void onDisable() {

    }

    /**
     * This renders all things that need to
     * be drawn in the game. In this case,
     * this calls the method
     * "drawBreadCrumbs", which actually
     * does the drawing.
     *
     * @param eventRender An event parameter for EventHandler.
     */
    @EventHandler
    public void onRender(EventRender eventRender) {
        if (this.isToggled() && this.positions.size() > 0) {
            this.drawBreadCrumbs();
        }
    }

    /**
     * This runs every tick, and is used to
     * add positions to the ArrayList,
     * which is then iterated through and
     * points are drawn at each position.
     *
     * @param eventUpdate An event parameter for EventHandler.
     */
    @EventHandler
    public void onUpdate(EventUpdate eventUpdate) {
        if (this.getValue("clear").getBoolean()[0]) {
            this.positions.clear();
            getValue("clear").setInitial(false);
        }
        if (this.isToggled()) {
            double posX = Apolune.getInstance().getPlayer().posX;
            double posY = Apolune.getInstance().getPlayer().boundingBox.minY;
            double posZ = Apolune.getInstance().getPlayer().posZ;
            double xDiff = Math.pow(this.lastX - posX, 2);
            double yDiff = Math.pow(this.lastY - posY, 2);
            double zDiff = Math.pow(this.lastZ - posZ, 2);
            double dis = Math.sqrt(xDiff + yDiff + zDiff);
            if (dis >= 0.05) {
                this.positions.add(new Double[]{
                        posX, posY, posZ
                });
                this.lastX = posX;
                this.lastY = posY;
                this.lastZ = posZ;
            }
        }
    }

    /**
     * This iterates through the "positions"
     * ArrayList, and draws lines through
     * the positions.
     */
    private void drawBreadCrumbs() {
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glDisable(GL11.GL_LIGHTING);
        GL11.glDisable(2929);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glEnable(GL11.GL_LINE_SMOOTH);
        GL11.glDepthMask(false);
        GL11.glLineWidth(1.25F);
        double posX = RenderManager.renderPosX;
        double posY = RenderManager.renderPosY;
        double posZ = RenderManager.renderPosZ;
        float[] color = Apolune.getInstance().getUtils().getRGBFromHex(0xFF5500AA);
        GL11.glBegin(GL11.GL_LINE_STRIP);
        for (Double[] d : this.positions) {
            GL11.glColor4f(color[0], color[1], color[2], 0.9f);
            GL11.glVertex3d(d[0] - posX, d[1] - posY, d[2] - posZ);
        }
        GL11.glEnd();
        GL11.glDisable(GL11.GL_LINE_SMOOTH);
        GL11.glEnable(2929);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glDepthMask(true);
        GL11.glDisable(GL11.GL_BLEND);
    }

}
