package com.thepanthurr.apolune.module.toggles;

import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.event.EventHandler;
import com.thepanthurr.apolune.event.events.EventPreUpdate;
import com.thepanthurr.apolune.module.Category;
import com.thepanthurr.apolune.module.Toggle;
import org.lwjgl.input.Keyboard;

/**
 * ToggleSpider is a module that allows you to
 * scale walls if you are touching them..
 *
 * @author thePanthurr
 * @version 1.0.1
 * @since 5/19/13
 */
public class ToggleSpider extends Toggle {
    public ToggleSpider() {
        super("Spider", "Allows you to climb walls.", Category.MOVEMENT, Keyboard.KEY_NONE);
    }

    @Override
    protected void onRegister() {
        setValue("speed", new String[]{"move, mod"}, new Object[]{1, 10, 1, 1}, true);
    }

    @Override
    protected void onEnable() {

    }

    @Override
    protected void onDisable() {

    }

    /**
     * This runs every movement update and simply checks
     * to see if the player is touching a wall on any
     * side of him, if so, it moves him upward.
     *
     * @param eventPreUpdate An event parameter for EventHandler.
     */
    @EventHandler
    public void onPreUpdate(EventPreUpdate eventPreUpdate) {
        if (isToggled()) {
            if (Apolune.getInstance().getPlayer().isCollidedHorizontally
                    || (Apolune.getInstance().getPlayer().isCollidedVertically
                    && !Apolune.getInstance().getPlayer().onGround)) {
                Apolune.getInstance().getPlayer().motionY = 0.1 * getValue("speed").getFloat()[0];
            }
        }
    }
}
