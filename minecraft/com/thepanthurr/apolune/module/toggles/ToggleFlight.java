package com.thepanthurr.apolune.module.toggles;

import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.event.EventHandler;
import com.thepanthurr.apolune.event.events.EventUpdate;
import com.thepanthurr.apolune.module.Category;
import com.thepanthurr.apolune.module.Toggle;
import org.lwjgl.input.Keyboard;

/**
 * ToggleFlight is a class used for player movement.
 * It enables creative mode's fly mode, and sets a
 * custom speed to make it faster.
 *
 * @author thePanthurr
 * @version 1.0.1
 * @since 5/1/13
 */
public class ToggleFlight extends Toggle {

    public ToggleFlight() {
        super("Soar", "Allows you to fly.", Category.MOVEMENT, Keyboard.KEY_R);
    }

    @Override
    protected void onRegister() {
        setValue("speed", new String[]{"s, mod, modifier"}, new Object[]{5, 15, 1, 15}, true);
    }

    @Override
    protected void onEnable() {

    }

    @Override
    protected void onDisable() {
        Apolune.getInstance().getPlayer().capabilities.setFlySpeed(1 / 20);
        Apolune.getInstance().getPlayer().capabilities.isFlying = false;
    }

    /**
     * This method runs every tick and
     * changes the player's flight speed and motionY
     * to match the speed held by the 'speed' value.
     *
     * @param eventUpdate An event parameter for EventHandler.
     */
    @EventHandler
    public void onTick(EventUpdate eventUpdate) {
        if (isToggled()) {
            if (Apolune.getInstance().getPlayer().capabilities.getFlySpeed() != getValue("speed").getFloat()[0] / 20) {
                Apolune.getInstance().getPlayer().capabilities.setFlySpeed(getValue("speed").getFloat()[0] / 20);
            }

            Apolune.getInstance().getPlayer().capabilities.isFlying = true;

            if (Apolune.getInstance().getMinecraft().currentScreen == null) {
                if (Keyboard.isKeyDown(Apolune.getInstance().getGameSettings().keyBindJump.keyCode)) {
                    Apolune.getInstance().getPlayer().motionY = getValue("speed").getFloat()[0] / 4;
                }
                if (Keyboard.isKeyDown(Apolune.getInstance().getGameSettings().keyBindSneak.keyCode)) {
                    Apolune.getInstance().getPlayer().motionY = -(getValue("speed").getFloat()[0] / 4);
                }
            }
        }
    }
}
