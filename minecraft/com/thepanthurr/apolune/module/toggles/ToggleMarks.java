package com.thepanthurr.apolune.module.toggles;

import java.util.List;

import net.minecraft.src.Entity;
import net.minecraft.src.EntityLiving;
import net.minecraft.src.EntityLivingBase;
import net.minecraft.src.Item;
import net.minecraft.src.MathHelper;

import org.lwjgl.input.Keyboard;

import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.event.EventHandler;
import com.thepanthurr.apolune.event.events.EventPostUpdate;
import com.thepanthurr.apolune.event.events.EventPreUpdate;
import com.thepanthurr.apolune.module.Category;
import com.thepanthurr.apolune.module.Toggle;

/**
 * ToggleMarks is a fancy name for a BowAimbot,
 * it uses a projectile projection algorithm
 * to determine the pitch needed to fire the bow
 * to hit the target you are aiming at.
 *
 * @author thePanthurr/ThatNahr
 * @version 1.0.5
 * @since 4/24/13
 */
public class ToggleMarks extends Toggle {

    private long start = 0L, end = 1L;  //Variables used for timing the hack.
    private float curYaw, curPitch;    //Variables used for holding the Player's current yaw and pitch, used for silent aimbot.

    public ToggleMarks() {
        super("Marks", "Aims your bow at the nearest entity to your cursor, and hits it.",
                Category.COMBAT, Keyboard.KEY_Y);
    }

    @Override
    protected void onRegister() {
        setValue("view", new String[]{"sight", "scope", "v"}, new Object[]{90, 180, 45, 90}, true);
        setValue("range", new String[]{"distance", "r"}, new Object[]{35, 120, 10, 120}, true);
        setValue("silent", new String[]{"sil", "sa", "aim"}, new Object[]{false, true, false, true}, true);
    }

    @Override
    protected void onEnable() {

    }

    @Override
    protected void onDisable() {

    }

    /**
     * This method runs every tick, and aims the bow
     * to the correct Entity while using a bow.
     *
     * @param eventPreUpdate An event parameter for EventHandler.
     */
    @EventHandler
    public void onPreUpdate(EventPreUpdate eventPreUpdate) {
        if (isToggled()) {
            curPitch = Apolune.getInstance().getPlayer().rotationPitch;
            curYaw = Apolune.getInstance().getPlayer().rotationYaw;
            start = System.nanoTime() / 1000000;
            if (Apolune.getInstance().getPlayer().isUsingItem()
                    && Apolune.getInstance().getPlayer().getCurrentEquippedItem().itemID == Item.bow.itemID
                    && getEntity(getValue("view").getInt()[0]) != null
                    && !Apolune.getInstance().getSaveFriends()
                    .isFriend(getEntity(getValue("view").getInt()[0]).getEntityName())) {
                faceEntity(getEntity(getValue("view").getInt()[0]));
                end = System.nanoTime() / 1000000;
            }
        }
    }

    /**
     * This method runs every tick, makes the rotation
     * of the player appear normal, getting rid of
     * the aimbot, client-side.
     *
     * @param eventPostUpdate An event parameter for EventHandler.
     */
    @EventHandler
    public void onPostUpdate(EventPostUpdate eventPostUpdate) {
        if (this.isToggled()) {
            if (getValue("silent").getBoolean()[0]) {
                Apolune.getInstance().getPlayer().rotationPitch = curPitch;
                Apolune.getInstance().getPlayer().rotationYaw = curYaw;
            }
        }
    }

    /**
     * This method return the closest Entity
     * to your cursor.
     *
     * @param rotation The largest rotation that the player is allowed
     *                 to turn. Based off of Ramisme's method.
     * @return entity
     *         The entity that is closest to your camera.
     */
    private Entity getEntity(int rotation) {
        Entity entity = null;
        double distance = rotation;
        for (Entity entity2 : (List<Entity>) Apolune.getInstance().getWorld().getLoadedEntityList()) {
            if (!(entity2 instanceof EntityLivingBase)) {
                continue;
            }
            EntityLivingBase entity1 = (EntityLivingBase) entity2;
            if (Apolune.getInstance().getPlayer().getDistanceToEntity(entity1) <= getValue("range").getInt()[0]
                    && Apolune.getInstance().getPlayer().canEntityBeSeen(entity1)
                    && !(Apolune.getInstance().getPlayer() == entity1)) {
                double curDistance = getDistanceBetweenAngles(getYaw(entity1), Apolune.getInstance().getPlayer().rotationYaw);
                if (curDistance < distance) {
                    entity = entity1;
                    distance = curDistance;
                }
            }
        }
        return entity;
    }

    /**
     * This method converts the angles to an angle less than 180.
     *
     * @param angle  The angle where the method starts counting.
     * @param angle1 The angle where the method stops counting.
     * @return diff
     *         The variable that is set to the difference
     *         between the 2 angles made to be less than 180.
     */
    private float getDistanceBetweenAngles(float angle, float angle1) {
        float diff = (Math.abs(angle - angle1)) % 360;
        if (diff > 180) {
            diff = (360 - diff);
        }
        return diff;
    }

    /**
     * This method turns the target player to face the Entity
     * in yaw, and to face the correct Pitch in order for the player
     * to hit the entity with the current power bow shot.
     *
     * @param entity The entity that the user is to look it.
     */
    private void faceEntity(Entity entity) {
        Apolune.getInstance().getPlayer().rotationPitch = getPitch(entity);
        Apolune.getInstance().getPlayer().rotationYaw = getYaw(entity);
    }

    /**
     * This method determines the yaw needed to
     * look at an Entity.
     *
     * @param entity The entity which the method will
     *               calculate the appropriate yaw for.
     * @return Math.toDegrees(Math.atan2(z, x)) - 90
     *         The way of determining the yaw to look at the
     *         entity.
     */
    private float getYaw(Entity entity) {
        double x = entity.posX - Apolune.getInstance().getPlayer().posX;
        double z = entity.posZ - Apolune.getInstance().getPlayer().posZ;

        return (float) Math.toDegrees(Math.atan2(z, x)) - 90;
    }

    /**
     * This method determines the pitch needed to
     * hit an Entity at position X,Y,Z with
     * a constant velocity and gravity.
     *
     * @param entity The entity which the method will
     *               calculate the appropriate pitch for.
     * @return rangle
     *         The calculated angle for a low trajectory
     *         after determining the course of action when
     *         reaching an NaN value.
     */
    public float getPitch(Entity entity) {
        double xdist = entity.posX - Apolune.getInstance().getPlayer().posX;
        double ydist = (entity.posY + (entity.getEyeHeight() - 0.3) - Apolune.getInstance().getPlayer().posY);
        double zdist = entity.posZ - Apolune.getInstance().getPlayer().posZ;
        float dist = (float) Math.sqrt(xdist * xdist + zdist * zdist);
        float vel = getVelocity();
        float grav = getGravity();
        float sqrt = (vel * vel * vel * vel) - (float) (grav * (grav * (dist * dist) + 2F * ydist * (vel * vel)));
        sqrt = MathHelper.sqrt_float(sqrt);
        float rangle = -(float) Math.toDegrees(Math.atan(((vel * vel) - sqrt) / (grav * dist)));
        if (Float.isNaN(rangle)) {
            rangle = (float) (-Math.toDegrees(Math.atan(ydist / dist)));
        }
        return rangle;
    }

    /**
     * This method returns the current velocity
     * that the arrow would be shot at if the bow
     * was released.
     *
     * @return pow
     *         The variable that is equivalent to the
     *         initial velocity of the Arrow calculated using
     *         Minecraft's algorithm.
     */
    public float getVelocity() {
        float pow = ((float) Apolune.getInstance().getPlayer().getItemInUseDuration()) / 20f;
        pow = (pow * pow + pow * 2.0F) / 3.0F;
        if (pow < 0.1F) {
            pow = 0.1F;
        }
        if (pow > 1.0F) {
            pow = 1.0F;
        }
        return pow;
    }

    /**
     * This method returns the gravity constant
     * of Minecraft.
     *
     * @return 0.006F
     *         The gravity constant in Minecraft for arrows.
     */
    public float getGravity() {
        return 0.006F;
    }

}
