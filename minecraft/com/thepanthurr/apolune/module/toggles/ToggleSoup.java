package com.thepanthurr.apolune.module.toggles;

import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.event.EventHandler;
import com.thepanthurr.apolune.event.events.EventPostUpdate;
import com.thepanthurr.apolune.event.events.EventPreUpdate;
import com.thepanthurr.apolune.event.events.EventUpdate;
import com.thepanthurr.apolune.module.Category;
import com.thepanthurr.apolune.module.Toggle;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import net.minecraft.src.Packet15Place;
import net.minecraft.src.Packet16BlockItemSwitch;
import org.lwjgl.input.Keyboard;

/**
 * ToggleSoup is a Module that focuses on allowing the
 * user to eat soup at incredible speeds, allowing for
 * near-invincibility depending on server.
 *
 * @author thePanthurr
 * @version 1.0.2
 * @since 4/25/13
 */
public class ToggleSoup extends Toggle {

    private int curItem = 1;    //A variable holding the item slot that the player had when he started switching to soup.
    private long start = 1L;    //A variable for holding the start time of the AutoSoup, used for delay.
    private long end = -1L;     //A variable for holding the end time of the AutoSoup, used for delay.

    public ToggleSoup() {
        super("Soup", "On KitPvP servers, automatically eats soup for you.", Category.COMBAT, Keyboard.KEY_H);
    }

    @Override
    protected void onRegister() {
        setValue("update", new String[]{"speed", "soups", "u", "s"}, new Object[]{5, 30, 1, 30}, true);
    }

    @Override
    protected void onEnable() {

    }

    @Override
    protected void onDisable() {

    }

    /**
     * This method runs before the update occurs
     * and sets the 'curItem' value to our current
     * item.
     *
     * @param eventPreUpdate An event parameter for EventHandler.
     */
    @EventHandler
    public void onPreUpdate(EventPreUpdate eventPreUpdate) {
        if (isToggled()) {
            curItem = Apolune.getInstance().getPlayer().inventory.currentItem;
        }
    }

    //TODO: Make bowls stack, make autosoup use shift clicks.
    /**
     * This method runs every tick, it checks
     * the delay, and if the delay allows, it
     * will switch to a bowl of soup.
     *
     * @param eventUpdate An event parameter for EventHandler.
     */
    @EventHandler
    public void onUpdate(EventUpdate eventUpdate) {
        if (isToggled()) {
            start = System.nanoTime() / 1000000;
            if (Apolune.getInstance().getPlayer().func_110143_aJ() < 16 && isDelayed()) {
                for (int i = 44; i >= 9; i--) {
                    ItemStack items = Apolune.getInstance().getPlayer().inventoryContainer.getSlot(i).getStack();
                    if (items != null) {
                        if (items.itemID == Item.bowlSoup.itemID) {
                            if (i >= 36 && i <= 44) {
                                Apolune.getInstance().getNetHandler().addToSendQueue(new Packet16BlockItemSwitch(i - 36));
                                Apolune.getInstance().getNetHandler().addToSendQueue(new Packet15Place(-1, -1, -1, -1, Apolune.getInstance().getPlayer().inventory.getCurrentItem(), 0, 0, 0));
                            } else {
                                Apolune.getInstance().getPlayerController().windowClick(0, 44, 0, 0, Apolune.getInstance().getPlayer());
                                Apolune.getInstance().getPlayerController().windowClick(0, i, 0, 0, Apolune.getInstance().getPlayer());
                                Apolune.getInstance().getPlayerController().windowClick(0, 44, 0, 0, Apolune.getInstance().getPlayer());
                                Apolune.getInstance().getNetHandler().addToSendQueue(new Packet15Place(-1, -1, -1, -1, Apolune.getInstance().getPlayer().inventory.getCurrentItem(), 0, 0, 0));
                            }
                        }
                    }
                }
                end = System.nanoTime() / 1000000;
            }
        }
    }

    /**
     * This method runs every post update and
     * sets our current item to our 'curItem' value.
     *
     * @param eventPostUpdate An event parameter for EventHandler.
     */
    @EventHandler
    public void onPostUpdate(EventPostUpdate eventPostUpdate) {
        if (isToggled()) {
            Apolune.getInstance().getNetHandler().addToSendQueue(new Packet16BlockItemSwitch(curItem));
        }
    }

    /**
     * This method checks the delay to make sure that its okay to
     * drink another bowl of soup.
     *
     * @return start - end >= 1000 / update + 2
     */
    public boolean isDelayed() {
        return start - end >= (long) (1000 / getValue("update").getInt()[0] + 2);
    }

}
