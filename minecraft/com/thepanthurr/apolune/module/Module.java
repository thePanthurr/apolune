package com.thepanthurr.apolune.module;

/**
 * Module is an interface used for marking Modules
 * that can be added or removed from the client at will,
 * without having other functionality be removed.
 *
 * @author thePanthurr
 * @version 1.0.0
 * @since 4/14/13
 */
public interface Module {

    /**
     * Used to get the name of the Module for referencing
     * in numerous places.
     *
     * @return The name of the Module.
     */
    public abstract String getName();
}
