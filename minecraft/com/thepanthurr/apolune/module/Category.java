package com.thepanthurr.apolune.module;

/**
 * Category is an enum used to categorize hacks,
 * so that they can later be displayed on the
 * gui and ArrayList accordingly.
 *
 * @author thePanthurr/ThatNahr
 * @version 1.0.5
 * @since 4/14/13
 */
public enum Category {

    /**
     * Used to define all hacks that apply directly to the players movement.
     */
    MOVEMENT("Movement", 0x0066FF),

    /**
     * Used to define all hacks that apply to the world being rendered around them.
     */
    WORLD("World", 0x00CC2F),

    /**
     * Used to define all hacks that apply to the alteration of the players combat capabilities.
     */
    COMBAT("Combat", 0x8800DD),

    /**
     * Used to define all hacks that apply to the modification of the Client itself.
     */
    CLIENT("Client", 0xACAFBD);

    String name;    //Used to define the name of the enum value.
    int color;      //Used to define the color of the hacks in the ArrayList with this enum value.

    private Category(String name, int color) {
        this.name = name;
        this.color = color;
    }

    /**
     * Used to return the display name of the enum value.
     * It is displayed in the GUI as a window title.
     *
     * @return name
     *         The name of the enum value.
     */
    public String getName() {
        return name;
    }

    /**
     * Used to return the hexadecimal color code of the enum
     * value. It is displayed in the ArrayList as this color
     * of text.
     *
     * @return color
     *         The hexadecimal color code set in the enum value.
     */
    public int getColor() {
        return color;
    }
}
