package com.thepanthurr.apolune.module;

import com.thepanthurr.apolune.Apolune;

/**
 * Command - Command is a base class used to handle the
 * groundwork for each chat/console command.
 *
 * @author thePanthurr
 * @version 1.0.7
 * @since 4/14/13
 */
public abstract class Command implements Module {

    private String[] names;             //Used to define what name the command is called by, internally, and in the game.
    private Object[] args;              //Used to hold parameters for the command, will be integrated into the usage.
    private String description;         //Used to describe the command to the user. Allows for a more friendly UX.
    private Highlight highlighting;     //Used to show how parameters are displayed in the usage.
    private String prefix = ".";        //Used to hold the prefix of the command, which will need to by typed in the chat box.

    public Command(String[] names, Object[] args, String description, Highlight highlighting) {
        this.names = names;
        this.args = args;
        this.description = description;
        this.highlighting = highlighting;
    }

    /**
     * Is called when a player types a message into the chatbox.
     * This method verifies if the command is intended to modify a hack,
     * and if so, passes the Hack argument to the run() method.
     *
     * @param message passed from the CommandManager, it is the message sent
     *                in the chat box by thePlayer.
     * @return true - if the command was executed successfully.
     *         || false - if the command ran into an error when running.
     */
    public boolean onCommand(String message) {
        try {
            String msgs[] = message.split(" ");
            if (isCommand(msgs[0])) {
                if (msgs.length > 1) {
                    for (Module module : Apolune.getInstance().getModuleManager().getModuleList()) {
                        if (module.getName().toLowerCase().equalsIgnoreCase(msgs[1])) {
                            run(msgs, module);
                            return true;
                        }
                    }
                }

                run(msgs, null);
                return true;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return false;
    }

    /**
     * Implemented by every Command, used to customize the actions
     * of each individual command.
     *
     * @param msgs   Passed from onCommand(), its a String array holding each individual
     *               word from the message entered in the chat box.
     * @param module Passed from onCommand(), it is passed as a Hack if the command was
     *               detected to modify a hack. Otherwise, it it passed as null.
     */
    protected abstract void run(String msgs[], Module module);

    /**
     * Used to retrieve an automagical usage, created from the command name
     * combined with each parameter for the command, utilizes the 'highlighting'
     * variable in order to choose the way parameters are displayed.
     *
     * @return theUsage
     *         The string that results by combining the prefix, then the name,
     *         and adding each parameter after it surrounded by the selected
     *         special characters.
     */
    public String getUsage() {
        String theUsage = prefix + names[0] + " ";

        if (args != null) {
            for (Object object : args) {
                theUsage += highlighting.getChars().substring(0, 1) + object.toString()
                        + highlighting.getChars().substring(1, highlighting.getChars().length()) + " ";
            }
        }

        theUsage = theUsage.substring(0, theUsage.length() - 1);
        return theUsage;
    }

    @Override
    public String getName() {
        return names[0];
    }

    /**
     * A method used to get the description for the given Command
     * for use with UX and user help.
     *
     * @return description
     *         The variable holding the short description of what
     *         the command does.
     */
    public String getDescription() {
        return description;
    }

    /**
     * A method used for determining if the command is
     * being called or not. Loops through the names for
     * the command and if one is found that matches,
     * it returns true.
     *
     * @param message The String to scan for instances of the commands name.
     * @return true if the command is found
     *         || false if the command is not.
     */
    public boolean isCommand(String message) {
        for (String string : names) {
            if (message.equalsIgnoreCase(prefix + string)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Highlight - Highlight is an enum used to define the special
     * characters surrounding a command. It can be [], <>, or ().
     *
     * @author thePanthurr
     * @version 1.0.1
     * @since April 12, 2013
     */
    protected enum Highlight {
        SQUARE("[]"),   //Displays brackets around the parameters. [args].
        TRIANGLE("<>"), //Displays greater/less than symbols around the parameters. <args>. 
        CIRCLE("()");   //Displays parentheses around the parameters. (args).
        String chars;   //Used to hold the special characters for the parameter highlighting.

        private Highlight(String chars) {
            this.chars = chars;
        }

        /**
         * Used to get the special characters surrounding each
         * parameter.
         *
         * @return chars
         *         The special characters that will be displayed in getUsage().
         */
        public String getChars() {
            return chars;
        }
    }
}