package com.thepanthurr.apolune.module.commands;

import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.module.Command;
import com.thepanthurr.apolune.module.Module;
import com.thepanthurr.apolune.module.Toggle;

/**
 * CommandSet is a class used to handle the setting
 * of Value's for each Module.
 *
 * @author thePanthurr/ThatNahr
 * @version 1.0.7
 * @since 4/22/13
 */
public class CommandSet extends Command {

    public CommandSet() {
        super(new String[]{"set", "="}, new Object[]{"module", "option", "number"},
                "Sets an option for a hack to the number specified.", Highlight.SQUARE);
    }

    @Override
    protected void run(String[] msgs, Module module) {
        Toggle toggle = (Toggle) module;
        if (toggle.getValue(msgs[2]) != null) {
            if (toggle.getValue(msgs[2]).isBoolean()
                    || (!(Float.parseFloat(msgs[3]) > toggle.getValue(msgs[2])
                    .getFloat()[((Toggle) Apolune.getInstance().getUtils().getModuleByName("NoCheat")).isToggled() ? 3 : 1]))) {
                Apolune.getInstance().getUtils().addChat(toggle.getName() + "'s " + msgs[2] + " has been set to : " + msgs[3] + "!");
                toggle.getValue(msgs[2]).setInitial(msgs[3]);
            } else {
                Apolune.getInstance().getUtils().addChat(msgs[2] + " cannot be set that high.");
            }

            toggle.toggle();
            toggle.toggle();
            Apolune.getInstance().getSaveValues().save();
        } else {
            Apolune.getInstance().getUtils().addChat("There is no option by that name.");
        }
    }
}
