package com.thepanthurr.apolune.module.commands;

import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.module.Command;
import com.thepanthurr.apolune.module.Module;
import com.thepanthurr.apolune.module.Toggle;
import com.thepanthurr.apolune.module.Value;
import org.lwjgl.input.Keyboard;

import java.text.DecimalFormat;

/**
 * CommandHelp is a Command for allowing the User
 * to get quick information about a given Module..
 *
 * @author thePanthurr/ThatNahr
 * @version 1.0.6
 * @since 4/23/13
 */
public class CommandHelp extends Command {

    public CommandHelp() {
        super(new String[]{"help"}, new Object[]{"module"},
                "Displays information about the Hack or Command.", Highlight.SQUARE);
    }

    @Override
    protected void run(String[] msgs, Module module) {
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        if (module instanceof Command) {
            Command curCommand = (Command) module;
            Apolune.getInstance().getUtils().addChat("------------------------");
            Apolune.getInstance().getUtils().addChat("[Command] : " + curCommand.getName());
            Apolune.getInstance().getUtils().addChat("[Description] : " + curCommand.getDescription());
            Apolune.getInstance().getUtils().addChat("[Usage] : " + curCommand.getUsage());
            Apolune.getInstance().getUtils().addChat("------------------------");

        } else if (module instanceof Toggle) {
            Toggle toggle = (Toggle) module;
            Apolune.getInstance().getUtils().addChat("------------------------");
            Apolune.getInstance().getUtils().addChat("[Module] : " + toggle.getName());
            Apolune.getInstance().getUtils().addChat("[Description] : " + toggle.getDescription());
            Apolune.getInstance().getUtils().addChat("[KeyBind] : " + Keyboard.getKeyName(toggle.getKeyBind())
                    + " - Can be set with the 'key' command.");

            for (Value value : toggle.getValues()) {
                Apolune.getInstance().getUtils().addChat("[" + Apolune.getInstance().getUtils().capitalize(value.getKey())
                        + "] : " + decimalFormat.format(value.getFloat()[0]) + " -  Can be set using the 'set' command.");
            }
            Apolune.getInstance().getUtils().addChat("------------------------");
        } else {
            Apolune.getInstance().getUtils().addChat("Correct usage is : " + getUsage());
        }

    }
}
