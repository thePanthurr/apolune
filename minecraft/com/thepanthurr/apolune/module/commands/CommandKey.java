package com.thepanthurr.apolune.module.commands;

import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.module.Command;
import com.thepanthurr.apolune.module.Module;
import com.thepanthurr.apolune.module.Toggle;
import org.lwjgl.input.Keyboard;

/**
 * CommandKey is a Command used for binding keys to hacks.
 *
 * @author thePanthurr
 * @version 1.0.3
 * @since 4/23/13
 */
public class CommandKey extends Command {

    public CommandKey() {
        super(new String[]{"key", "bind"}, new Object[]{"module", "key"},
                "Binds a key to the specified Module.", Highlight.SQUARE);
    }

    @Override
    protected void run(String[] msgs, Module module) {
        Toggle toggle = (Toggle) module;
        toggle.setKeyBind(Keyboard.getKeyIndex(msgs[2].toUpperCase()));
        Apolune.getInstance().getUtils().addChat(toggle.getName() + " has been successfully bound to : " + msgs[2].toUpperCase() + ".");
        Apolune.getInstance().getSaveKeys().save();
    }
}
