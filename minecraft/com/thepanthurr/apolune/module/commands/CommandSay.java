package com.thepanthurr.apolune.module.commands;

import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.module.Command;
import com.thepanthurr.apolune.module.Module;
import net.minecraft.src.Packet3Chat;

/**
 * CommandSay is a way of type .things in chat so that
 * you can avoid detection of server admins.
 *
 * @author thePanthurr
 * @version 1.0.0
 * @since 6/12/13
 */
public class CommandSay extends Command {

    public CommandSay() {
        super(new String[]{"say"}, new Object[]{"message"}, "Allows you to send a message beginning with .", Highlight.SQUARE);
    }

    @Override
    protected void run(String[] msgs, Module module) {
        String s = "";

        for (String string : msgs) {
            s += (string + " ");
        }

        String msg = s.substring(4, s.length());
        Apolune.getInstance().getNetHandler().addToSendQueue(new Packet3Chat(msg));
    }
}
