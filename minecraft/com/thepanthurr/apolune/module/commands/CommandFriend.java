package com.thepanthurr.apolune.module.commands;

import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.module.Command;
import com.thepanthurr.apolune.module.Module;

/**
 * CommandFriend is a command used to add or delete
 * friends from the ArrayList held in SaveFriend.
 *
 * @author thePanthurr
 * @version 1.0.0
 * @since 5/13/13
 */
public class CommandFriend extends Command {

    public CommandFriend() {
        super(new String[]{"friend", "friends"}, new Object[]{"name"},
                "Add or remove friends so that your pvp hacks won't attack them!", Highlight.SQUARE);
    }

    @Override
    protected void run(String[] msgs, Module module) {
        if (msgs[1].equalsIgnoreCase("add")) {
            if (!Apolune.getInstance().getSaveFriends().getFriendsList().contains(msgs[2])) {
                Apolune.getInstance().getSaveFriends().getFriendsList()
                        .add(Apolune.getInstance().getUtils().stripColorCodes(msgs[2].toLowerCase()));
                Apolune.getInstance().getUtils().addChat("Friend successfully added : " + msgs[2]);
            } else {
                Apolune.getInstance().getUtils().addChat(msgs[2] + " is already a friend.");
            }
        }

        if (msgs[1].equalsIgnoreCase("del")
                || msgs[1].equalsIgnoreCase("delete")
                || msgs[1].equalsIgnoreCase("remove")) {
            if (Apolune.getInstance().getSaveFriends().getFriendsList().contains(msgs[2])) {
                Apolune.getInstance().getSaveFriends().getFriendsList()
                        .remove(Apolune.getInstance().getUtils().stripColorCodes(msgs[2].toLowerCase()));
                Apolune.getInstance().getUtils().addChat("Friend successfully removed : " + msgs[2]);
            } else {
                Apolune.getInstance().getUtils().addChat(msgs[2] + " was not a friend anyway.");
            }
        }
        Apolune.getInstance().getSaveFriends().save();
    }
}
