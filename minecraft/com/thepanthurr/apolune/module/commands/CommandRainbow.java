package com.thepanthurr.apolune.module.commands;

import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.module.Command;
import com.thepanthurr.apolune.module.Module;

/**
 * CommandRainbow is a Command used for testing out the
 * chat colors with different fonts.
 *
 * @author thePanthurr
 * @version 1.0.3
 * @since 4/21/13
 */
public class CommandRainbow extends Command {

    public CommandRainbow() {
        super(new String[]{"rainbow", "colors"}, null, "Prints out chat messages with each color code.", Highlight.SQUARE);
    }

    @Override
    protected void run(String[] msgs, Module module) {
        String colors = "0123456789abcdefq";
        for (char theCharacter : colors.toCharArray()) {
            Apolune.getInstance().getUtils().addChat("\247" + theCharacter + "Color Test [ " + theCharacter + " ].");
        }
    }
}
