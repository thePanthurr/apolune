package com.thepanthurr.apolune.module.commands;

import com.thepanthurr.apolune.Apolune;
import com.thepanthurr.apolune.module.Command;
import com.thepanthurr.apolune.module.Module;

/**
 * CommandVClip is a command used to teleport
 * the player vertically, whether it be up or down.
 *
 * @author ThatNahr
 * @version 1.0.3
 * @since 4/26/13
 */
public class CommandVClip extends Command {

    public CommandVClip() {
        super(new String[]{"vclip", "v", "up"}, new Object[]{"number"}, "Allows the player to teleport up and down.", Highlight.SQUARE);
    }

    @Override
    protected void run(String[] msgs, Module module) {
        try {
            float number = Float.parseFloat(msgs[1]);
            if (number < -8f) {
                number = -8f;
            }
            if (number > 8f) {
                number = 8f;
            }
            if (number != 0) {
                Apolune.getInstance().getPlayer().setPositionAndUpdate(Apolune.getInstance().getPlayer().posX,
                        Apolune.getInstance().getPlayer().boundingBox.minY + number,
                        Apolune.getInstance().getPlayer().posZ);
                Apolune.getInstance().getUtils().addChat("Attemped to teleport " + number + " blocks.");
            }
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
            Apolune.getInstance().getUtils().addChat("Wrong syntax! Use numbers!");
        }
    }

}
